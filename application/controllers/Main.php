<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->search=isset($_GET['search'])?$_GET['search']:FALSE;
		$this->load->model('default/m_seo');
		$this->load->model('default/m_category');
	}
	public function index()
	{
		$this->data['meta']  = $this->m_seo->getSEO(1);
		$this->data['banners'] = $this->M_myweb->set_table('home_banner')->sets(array('deleted'=>0,'active'=>1))->gets();
		$categories = $this->M_myweb->set_table('category')->sets(array('deleted'=>0,'active'=>1,'level'=>0))->gets();
		// $sub_cate = array();
		// foreach($categories as $item){
		// 	$sub_cate[] = $this->M_myweb->set_table('category')->sets(array('deleted'=>0,'active'=>1,'parent'=>$item->id))->gets();
		// }
		// // print_r($sub_cate);
		// $this->data['sub_cate'] = $sub_cate;
		$this->data['cats'] = $categories;
		$this->data['products'] = $this->M_myweb->set_table('product')->sets(array('deleted'=>0,'active'=>1,'hot'=>1))->gets();
		$this->data['hotproducts'] = $this->M_myweb->set_table('home_hot_product')->gets()[0];
		$this->data['partner'] = $this->M_myweb->sets(array('deleted'=>0,'active'=>1))->set_table('partner')->gets();
		// print_r($this->data['partner']);
		$this->data['com_info'] = $this->M_myweb->set_table('home_info_compa')->gets()[0];
		$this->data['title']	= "Trang Chủ";
		$this->data['subview'] 	= 'default/index/V_index';
		$this->Model = $this->M_myweb->set_table('about');
		$introduce_info = $this->Model->get();
		$this->data['introduce_info'] = $introduce_info;

		$this->Model = $this->M_myweb->sets(array('deleted'=>0,'active'=>1,'parent <>'=>0))->set_pageof(10)->page(1)->set_table('news');
		$news = $this->Model->gets();
		$this->data['news'] = $news;

		$this->Model = $this->M_myweb->set_table('home_info_compa');
		$pricetable = $this->Model->get();
		$this->data['pricetable'] = $pricetable;
		// print_r($news);
		$this->load->view('default/_main_page',$this->data);
	}
	public function search()
	{
		if($this->search)
		{
			$this->data['meta']  = $this->m_seo->getSEO(5);
			$search=explode(" ",$this->search);
			if(count($search)<2){
				$this->data['title']="Tìm Kiếm";
				$this->data['error']="Vui lòng nhập nhiều hơn 1 từ";
				$this->data['subview'] 	= 'default/search/V_search';
				$this->load->view('default/_main_page',$this->data);
			}
			else{
				$this->data['title']="Tìm Kiếm";
				$this->data['search']=$this->m_nguyenquan->getSearchData($this->search);
				$this->data['subview'] 	= 'default/search/V_search';
				$this->load->view('default/_main_page',$this->data);
			}

		}
		else
		{
			redirect(site_url());
		}
	}
	public function partner_registry()
	{
		$data=$this->input->post();
		if($data)
		{
			$this->M_myweb->set_table('partner_registry')->sets($data)->save();
			echo 1;
		}
		else{
			echo 0;
		}
	}
}
