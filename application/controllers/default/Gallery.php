<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gallery extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->gallery_cate = isset($_GET['gallery_cate']) ? $this->input->get('gallery_cate') : false;
		$this->M_myweb->set_table('gallery');
		$this->load->model('default/m_gallery');
		$this->load->model('default/m_seo');
        $this->data['partner'] = $this->M_myweb->sets(array('deleted'=>0,'active'=>1))->set_table('partner')->gets();

	}

	public function index()
	{
		$this->data['meta']  = $this->m_seo->getSEO(3);
		$this->data['gallery_datas'] = $this->m_gallery->getGalleryList();
		$this->data['title'] = "Hình Ảnh";
		$this->data['subview'] = 'default/gallery/V_gallery';
		$this->load->view('default/_main_page', $this->data);
	}
	public function album($album)
	{
		$this->data['album']=$album;
		$this->data['gallery_data'] = $this->m_gallery->getGalleryWhere(array('slug'=>$album));
		$this->data['images'] = $this->m_gallery->loadImagePage($this->data['gallery_data']->id);
		if (!empty($this->data['images'])){
		$this->data['title'] = "Hình Ảnh";
		$this->data['subview'] = 'default/gallery/V_gallery';
		$this->load->view('default/_main_page', $this->data);
		}else{
			redirect ('gallery');
		}

	}
	public function getPageAjax()
	{
		$page = $_POST['page'];
		if(isset($page))
		{
			$category = $_POST['category']!=0?$_POST['category']:false;
			$this->m_gallery->setPage($page);
			$this->data['images'] = $this->m_gallery->loadImagePage($category);
			if($this->data['images'])
			{
				$this->load->view('default/gallery/V_gallery_page',$this->data);
			}
		}else{
			redirect(site_url('/gallery'));
		}
	}
}