<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta']  = $this->M_myweb->set('id',5)->set_table('SEO')->get();
		$this->data['partner'] = $this->M_myweb->sets(array('deleted'=>0,'active'=>1))->set_table('partner')->gets();

		$this->Model = $this->M_myweb->set_table('about');
	}
	
	public function index()
	{
		//general
		$introduce_info = $this->Model->get();
		$this->data['introduce_info'] = $introduce_info;
		//history
		$this->Model = $this->M_myweb->set('deleted',0)->set_table('history');
		$this->data['history'] 	= $this->Model->set_orderby('title')->gets();
		$this->data['title']	= "Về Chúng Tôi";
		$this->data['subview'] 	= 'default/about/V_about';
		// print_r($this->data['introduce_info']);
		$this->load->view('default/_main_page',$this->data);
	}
}