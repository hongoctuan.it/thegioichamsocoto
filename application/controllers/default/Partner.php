<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('partner');
		$this->load->model('default/m_seo');
		$this->data['partner'] = $this->M_myweb->sets(array('deleted'=>0,'active'=>1))->set_table('partner')->gets();

	}
	
	public function index(){
		$this->data['meta']  = $this->m_seo->getSEO(4);
		$partner = $this->Model->sets(array('deleted'=>0,'active'=>1))->set_orderby('name')->gets();
		$this->data['partner_nam'] = array();
		$this->data['partner_trung'] = array();
		$this->data['partner_bac'] = array();
		foreach($partner as $item){
			if($item->area == 0)
				$this->data['partner_nam'][]=$item;
			else if($item->area == 1)
				$this->data['partner_trung'][]=$item;
			else
				$this->data['partner_bac'][]=$item;
		}
		$this->data['title']	= "Đối Tác";
		$this->data['subview'] 	= 'default/partner/V_partner';
		$this->load->view('default/_main_page',$this->data);
	}
}