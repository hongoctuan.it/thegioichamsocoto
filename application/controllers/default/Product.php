<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta']  = $this->M_myweb->set('id',2)->set_table('SEO')->get();
		$this->id = $this->input->get('id');
		$this->load->model('default/m_product');
		$this->data['partner'] = $this->M_myweb->sets(array('deleted'=>0,'active'=>1))->set_table('partner')->gets();

	}
	function _remap($slug) {
        $this->index($slug);
    }
	public function index($slug)
	{
		// $this->m_product->setWhere("product.id",$this->id);
		$this->m_product->setWhere("product.slug",$slug);
		$this->data['product'] = $this->m_product->getProductWhere();
		if($this->data['product'])
		{
			$this->data['relate_products']  = $this->M_myweb->set_table('product')->sets(array('category_id'=>$this->data['product']->category_id,'deleted'=>0,'active'=>1))->gets();
			$this->data['partners']=$this->M_myweb->set_table('product_partner')->set('product_id',$this->data['product']->id)->gets();
			if($this->data['partners'])
			{
				foreach($this->data['partners'] as $item)
				{
					$this->data['partner'][$item->id]=$this->M_myweb->set_table('partner')->set('id',$item->partner_id)->get();
				}
			}
			$this->data['title']	= "Sản Phẩm";
			$this->data['subview'] 	= 'default/product/V_productDetail';
			$this->load->view('default/_main_page',$this->data);
		}else{
			$this->data['title']	= "Sản Phẩm";
			$this->data['subview'] 	= 'default/product/V_noProduct';
			$this->load->view('default/_main_page',$this->data);
		}
	}
}