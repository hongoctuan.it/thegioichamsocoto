<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class homeInfoCompa extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('home_info_compa');
	}
	
	public function index()
	{
		
		switch($this->act){
			case "upd":
				$this->save();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data['obj'] = $this->Model->get();
		$this->data['subview'] = 'cms/hotproductinfo/homeInfoCompa';
		$this->load->view('cms/_main_page',$this->data);
  }
    
  private function save(){
		$data = $this->input->post();
		$image_04 = "";
		if($_FILES['image_01']['name']!=""){
			$image_01 = do_upload('price','image_01');	
			$data['pricetable'] = $image_01;	
		}
		$this->Model->sets($data)->setPrimary(1)->save();
		$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật thành công");
		return redirect(site_url('admin/homeInfoCompa'));
	}
}