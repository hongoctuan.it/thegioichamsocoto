<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotproductinfo extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('news');
	}
	
	public function index()
	{
		switch($this->act){
			case "upd":

				$this->save();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->Model = $this->M_myweb->set_table('home_hot_product');
		$this->data['obj'] = $this->Model->get();
		$this->data['subview'] = 'cms/hotproductinfo/desHotProduct';
		$this->load->view('cms/_main_page',$this->data);
	}

	

	private function save(){
		$this->Model = $this->M_myweb->set_table('home_hot_product');
		$data = $this->input->post();
		$image_01 = "";
		$arr= array();

		// if($_FILES['image_01']['name']!=""){
		// 	$image_01 = do_upload('home','image_01');	
		// 	$data['img']=$image_01;
		// 	var_dump($image_01);
		// }
		$data['img'] = (explode("watch?v=",$data['img']))[1];
		$this->Model->sets($data)->setPrimary(1)->save();
		$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật thành công");
		return redirect(site_url('admin/hotproductinfo'));
	}

	

	
}