<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('partner');
	}
	
	public function index()
	{	
		switch($this->act){
			case "new":
				$this->save();
				break;
			case "upd":
				$this->updatePartner();
				break;
			case "del":
				$this->delPartner();
				break;
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			case "registry-list":
				$this->partner_registry();
				break;
			// case "partner_info":
			// 	$this->partner_info();
			// 	break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data['partner'] = $this->Model->set('deleted',0)->set_orderby('image')->gets();
		$this->data['subview'] = 'cms/partner/home';
		$this->load->view('cms/_main_page',$this->data);
  }
    
  private function save(){
		$data = $this->input->post('Partner');
		if(!empty($_FILES['image_01']['name'])){
			$image_01 = "";
			if($_FILES['image_01']['name']!=""){
				$image_01 = do_upload('partner','image_01');	
				$data['image'] = $image_01;				
			}
			$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm thư viện thành công");
			return redirect(site_url('admin/partner'));
		}else{
			$this->data['subview'] = 'cms/partner/edit';
			$this->load->view('cms/_main_page',$this->data);
		}
	}

	private function updatePartner(){
		$data = $this->input->post('Partner');
		$id = $_GET['id'];
		if(!empty($data)){
			$data['slug'] = str_replace(" ","_",$data['slug']);
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật thư viện thành công");
			return redirect(site_url('admin/partner'));
		}else{
			if(isset($_GET['id'])){
				$this->data['id'] = $_GET['id'];
				$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
			}
			$this->data['subview'] = 'cms/partner/edit';
			$this->load->view('cms/_main_page',$this->data);
		}
	}
	private function delPartner(){
		$id = $_GET['id'];
		if($this->id){
			$getPro = $this->Model->set('id',$this->id)->get();
			if($getPro){
				$this->Model->sets(array('deleted'=>1))->setPrimary($this->id)->save();
				$_SESSION['system_msg'] = messageDialog("div","success","Xoá hình đối tác công");
			}else{
				$_SESSION['system_msg'] = messageDialog("div","error","Không thể xoá đối tác");
			}
		}
		return redirect(site_url('admin/partner'));
	}

	private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/partner/home';
		return redirect(site_url('admin/partner?id='.$_GET['id'].'&token='.$this->data['infoLog']->token));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/gallery/home';
		return redirect(site_url('admin/partner?id='.$_GET['id'].'&token='.$this->data['infoLog']->token));
			
	}
	private function partner_registry(){
		
		$this->data['partners'] = $this->M_myweb->set_table('partner_registry')->gets();
		$this->data['subview'] = 'cms/partner/registry';
		$this->load->view('cms/_main_page',$this->data);
	}

	// private function partner_info(){
	// 	$this->Model = $this->M_myweb->set_table('partner_info');
	// 	$data = $this->input->post('Partner_info');
	// 	if(!empty($data)){
	// 		$this->Model->sets($data)->setPrimary(1)->save();
	// 		$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật thư viện thành công");
	// 		return redirect(site_url('admin/partner?act=partner_info&token='.$this->data['infoLog']->token));
	// 	}else{
	// 		$this->data['obj'] = $this->Model->set('id',1)->get();
	// 		$this->data['subview'] = 'cms/partner/partner_info';
	// 		$this->load->view('cms/_main_page',$this->data);
	// 	}
	// }
}