<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//default

$route['search'] = 'main/search';
$route['contact'] = 'main/partner_registry';


$route['category'] = 'default/category';
$route['category/ajax/getpage'] = 'default/category/getpageajax';
$route['category/(:any)'] = 'default/category/category/$1';

$route['product'] = 'default/product';
$route['product/(:any)'] = 'default/product/$1';
$route['about'] = 'default/about';
$route['partner'] = 'default/partner';
$route['gallery'] = 'default/gallery';
$route['gallery/ajax/getpage'] = 'default/gallery/getpageajax';
$route['gallery/(:any)'] = 'default/gallery/album/$1';

$route['news'] = 'default/news';
$route['news/category/(:any)'] = 'default/news/category/$1';
$route['news/(:any)'] = 'default/news/details/$1';


//admin
$route['admin'] = 'cms/product';
$route['admin/login'] = 'auth';
$route['admin/logout'] = 'auth/logout';
$route['admin/(:any)'] = 'cms/$1';
$route['(:any)/(:any)'] = 'default/test/details/$1/$2';
$route['(:any)'] = 'default/test/detail/$1';