<?php
$action = $obj?site_url('admin/SEO?act=upd&id='.$obj->id."&token=".$infoLog->token):site_url('admin/SEO?act=upd&token='.$infoLog->token);

?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Thế Giới Chăm Sóc Ô Tô / SEO
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					SEO
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
						<input type="hidden" id="id" name="id" value="<?php echo $obj?$id:" " ?>">
						<div class="col-md-4">
							<div class="form-group required">
								<label class="control-label">Tiêu đề</label>
								<input type="text" class="form-control" name="title" id="name" required value="<?php echo $obj?$obj->title:" ";?>"/>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group required">
								<label class="control-label">Mô Tả</label>
								<textarea required type="text" class="form-control" rows=5 name="meta" id="description"><?php echo $obj?$obj->meta:"";?></textarea>
							</div>
						</div>
						

						<div class="clearfix"></div>
						<div class="col-md-3">
							<a class="btn btn-default" href="<?php echo site_url('admin/banner');?>">Quay lại</a>
							<button type="reset" class="btn btn-warning">Huỷ</button>
							<button type="submit" id="formSubmit" class="btn btn-primary">Gửi</button>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->


</div>
<!-- END: .app-main -->