<?php
	$action = site_url('admin/homeInfoCompa?act=upd&token='.$infoLog->token);
?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Thế Giới Chăm Sóc Ô Tô / Bảng Báo Giá
				</h6>
				<h3 class="dashhead-title">Bảng Báo Giá</h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Bảng Báo Giá
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<div class="col-md-12">
								<div class="form-group">
									<label>Bảng báo giá</label>
									<div>
										<input type="file" name="image_01">
									</div>
								</div>
							</div>
							<!-- <div class="col-md-6" >
								<div class="form-group required">
									<label class="control-label">Miền</label>
									<input type="text" class="form-control" name="area" id="area" required value="<?php echo $obj?$obj->area:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Tỉnh</label>
									<input type="text" class="form-control" name="district" id="district" required value="<?php echo $obj?$obj->district:"";?>"/>
								</div>

							</div>
                            <div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Đại lý</label>
									<input type="text" class="form-control" name="agency" id="agency" required value="<?php echo $obj?$obj->agency:"";?>"/>
								</div>
							</div>
                            <div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Sản phẩm</label>
									<input type="text" class="form-control" name="product" id="product" required value="<?php echo $obj?$obj->product:"";?>"/>
								</div>
	
							</div> -->
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin');?>">Quay lại</a>
								<button type="reset" class="btn btn-warning">Huỷ</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Lưu</button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->