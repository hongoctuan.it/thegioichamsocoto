<?php
	$action = site_url('admin/partner?act=partner_info&token='.$infoLog->token);
?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Thế Giới Chăm Sóc Ô Tô / Thông tin nhà phân phối
				</h6>
				<h3 class="dashhead-title">Quản lý thông tin nhà phân phối</h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Nhà phân phối / Quản lý thông tin nhà phân phối
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Tiêu đề</label>
									<input type="text" class="form-control" name="Partner_info[title]" id="name" required value="<?php if(isset($obj->title)) echo $obj->title;?>"/>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Mô tả</label>
									<textarea id="content" class="form-control" name="Partner_info[description]" required id="Partner_info[description]" ><?php echo $obj?$obj->description:"";?></textarea>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin/introduce');?>">Quay lại</a>
								<button type="reset" class="btn btn-warning">Huỷ</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Gửi</button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->