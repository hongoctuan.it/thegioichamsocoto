<?php
if(isset($id)){
	$action = site_url('admin/partner?act=upd&id='.$id.'&token='.$infoLog->token);
}else{
	$action = site_url('admin/partner?act=new&token='.$infoLog->token);
}
$image_01 = $obj && $obj->image != "" ? base_url('assets/partner/' . $obj->image) : base_url('assets/public/image_icon.png');

?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Thế Giới Chăm Sóc Ô Tô / Đối tác
				</h6>
				<h3 class="dashhead-title">Quản lý đối tác</h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Đối tác / Quản lý đối tác
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<div class="col-md-3">
								<div class="form-group">
									<label>Ảnh đại diện
									</label>
									<div>
										<img id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01?>" />
										<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							
							
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin/introduce');?>">Quay lại</a>
								<button type="reset" class="btn btn-warning">Huỷ</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Gửi</button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->