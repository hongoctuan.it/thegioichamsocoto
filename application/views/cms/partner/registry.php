<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Thế Giới Chăm Sóc Ô Tô / Đối tác
				</h6>
				<h3 class="dashhead-title">Danh sách đối tác chờ liên hệ</h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Đối tác / Danh sách đối tác chờ liên hệ
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				
				<div class="box-body">

					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>

					<table data-plugin="datatables" class="table table-striped table-bordered" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>#</th>
								<th>Người đại diện</th>
								<th>Công ty</th>
								<th>Số ĐT</th>
								<th>Email</th>
								<th>Ngày giờ liên hệ</th>
							</tr>
						</thead>
						<tbody>
						<?php if($partners):
							foreach($partners  as $key=>$obj){
						?>
							<tr>
								<td><?php echo $key+1?></td>
								<td>
									<?php echo $obj->representator?>
								</td>
								<td>
									<?php echo $obj->company?>
								</td>
								<td>
									<?php echo $obj->phone?>
								</td>
								<td>
									<?php echo $obj->email?>
								</td>
								<td>
									<?php echo date("d",strtotime($obj->register_date))." tháng ".date("m",strtotime($obj->register_date)).", ".date("Y G:i:s",strtotime($obj->register_date))?>
								</td>

							</tr>
						<?php } endif;?>
						</tbody>
					</table>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->
<!-- 
	</div> -->
	<!-- END: .main-content -->
	
	<?php if(checkaction($this->data['cslug'],'delete')){?>
	<div class="modal fade" id="deleteModal" tabindex="-2" role="dialog" aria-labelledby="deleteModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="deleteModalLabel">Xoá đối tác</h4>
				</div>
				<div class="modal-body">
					<div class="md-content">
						Bạn muốn xoá đối tác?      
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" id="closeCPModal">Đóng</button>
					<a href="#" id="confirmDelete" class="btn btn-primary">Xác nhận</a>
				</div>
			</div>
		</div>
	</div>
	<?php }?>
</div>
	</div>
<!-- END: .app-main -->