<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Thế Giới Chăm Sóc Ô Tô / Danh sách sản phẩm
				</h6>
				<h3 class="dashhead-title">Danh sách sản phẩm</h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Sản phẩm / Danh sách sản phẩm
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<?php if(checkaction($this->data['cslug'],'add')){?>
					<div class="p-b-15"><a href="<?php echo site_url('admin/product?act=upd&token='.$infoLog->token)?>" class="btn btn-primary pull-right"><span class="fa fa-fw fa-plus"></span> Thêm mới sản phẩm</a></div>
					<?php }?>
					<table data-plugin="datatables" class="table table-striped table-bordered" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>#</th>
								<th>Tên Sản Phẩm</th>
								<th>Loại Sản Phẩm</th>
								<!-- <th>Giá Sản Phẩm</th> -->
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php if($product):
							foreach($product as $key=>$obj){
						?>
							<tr>
								<td><?php echo $key+1?></td>
								<td>
									<?php echo $obj->name?>
								</td>
								<td><?php echo $obj->category?></td>
								<!-- <td>
									<?php echo $obj->price?>
								</td> -->
								<td>
									<a href="<?php echo site_url('admin/product?act=upd&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
										<span class="nav-icon">
											<i class="fa fa-fw fa-edit "></i>
										</span>
									</a>
									<!-- <?php if($obj->hot==1):?>
									<a href="<?php echo site_url('admin/product?act=unhot&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
										<span class="nav-icon">
											Hot
										</span>
									</a>
									<?php else:?>
									<a href="<?php echo site_url('admin/product?act=hot&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
										<span class="nav-icon">
											Un-Hot
										</span>
									</a>
									<?php endif;?> -->
									<?php if($obj->lock==1):?>
									<a href="<?php echo site_url('admin/product?act=lock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
										<span class="nav-icon">
											<i class="fa fa-fw fa-unlock-alt "></i>
										</span>
									</a>
									<?php else:?>
									<a href="<?php echo site_url('admin/product?act=unlock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
										<span class="nav-icon">
											<i class="fa fa-fw fa-lock" style="color:red"></i>
											
										</span>
									</a>
									<?php endif;?>
									<a href="javascript:void(0);" title="Delete" id="btndelete" module="product" data-id="<?php echo $obj->id?>" data-toggle="modal" data-target="#deleteModal">
										<span class="nav-icon">
											<i class="fa fa-fw fa-trash "></i>
										</span>
									</a>
								</td>
							</tr>
						<?php } endif;?>
						
						</tbody>
						
					</table>
					<?php echo isset($links)?$links:""; ?>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->
<!-- 
	</div> -->
	<!-- END: .main-content -->
	
	<?php if(checkaction($this->data['cslug'],'delete')){?>
	<div class="modal fade" id="deleteModal" tabindex="-2" role="dialog" aria-labelledby="deleteModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="deleteModalLabel">Xoá sản phẩm</h4>
				</div>
				<div class="modal-body">
					<div class="md-content">
						Bạn muốn xoá sản phảm?    
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" id="closeCPModal">Đóng</button>
					<a href="<?php echo site_url('admin/product?act=del&id='.$obj->id."&token=".$infoLog->token);?>" id="confirmDelete" class="btn btn-primary">Xác nhận</a>
				</div>
			</div>
		</div>
	</div>
	<?php }?>
</div>
	</div>
<!-- END: .app-main -->