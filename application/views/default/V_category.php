<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="statics/default/css/category.css">
    <link rel="stylesheet" href="statics/default/css/header.css">
    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.theme.default.min.css">
    <link
        href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese"
        rel="stylesheet">
    <title>Category</title>
</head>

<body>
    <?php include('include/V_menu.php'); ?>
    <section class="category-banner">
        <div class="category-banner-wrap text-center">
            <div class="category-banner-wrap-layer"></div>
            <h1 class="category-title dropdown-btn text-white text-center mx-auto">
                Category
                <i class="product-back-icon fas fa-arrow-down ml-3"></i>
            </h1>
            <ul class="category-list nav nav-pills mx-auto mt-lg-5" id="pills-tab" role="tablist">
                <?php foreach($cats as $cat):?>
                    <li class="category-list-item <?php if($cat->id == 1){echo 'active';}?>">
                        <a class="nav-link <?php if($cat->id == 1){echo 'active';}?> category-list-item-link" id="pills-home-tab" data-toggle="pill"
                            href="#pills-category<?php echo $cat->id;?>" role="tab" aria-controls="pills-category<?php echo $cat->id;?>" aria-selected="true">
                            <div class="row">
                                <div class="category-list-item-menu-logo">
                                    <i class="fas fa-utensils ml-1"></i>
                                </div>
                                <div>
                                    <h2 class="category-list-item-title text-left text-white active mr-1">
                                        <?php echo $cat->name ?></h2>
                                    <p class="category-list-item-product-quantity text-left text-white"><?php echo count($cat->product);?></p>
                                </div>
                            </div>
                        </a>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    </section>
    <section class="category-product my-3">
        <div class="container">
            <div class="row">
                <div class="tab-content col-xs-12 col-sm-12 col-md-12" id="pills-tabContent">
                    <?php foreach($cats as $cat):?>
                        <div class="tab-pane fade show <?php if($cat->id == 1){echo 'active';}?>" id="pills-category<?php echo $cat->id;?>" role="tabpanel"
                            aria-labelledby="pills-home-tab">
                            <div>
                                <h1 class="category-banner-title text-white text-center">
                                    <a href="" class="text-white">
                                        <i class="product-back-icon fas fa-arrow-left"></i>
                                    </a><?php echo $cat->name ?></h1>
                                <p style="text-align:center"><?php echo $cat->description ?></p>
                            </div>
                            <div class="row">
                                <?php if($cat->product):?>
                                    <?php foreach($cat->product as $item): ?>
                                        <div class="col-md-3 category-product-item text-center my-3">
                                            <a href="<?php echo site_url('product?id='.$item->id);?>">
                                                <figure class=figure>
                                                    <div class="category-product-item-img">
                                                        <img src="assets/public/<?php echo $item->img1 ?>" alt=""
                                                            class="img-fluid">
                                                    </div>
                                                    <figcaption>
                                                        <h2 class="category-product-item-title"><?php echo $item->name?></h2>
                                                        <p class="category-prodcut-item-desc"><?php echo $item->short_des?></p>
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </div>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <p class="mx-auto mb-5"><b>Không có sản phẩm</b><p>
                                <?php endif;?>
                            </div>
                        </div>
                    <?php endforeach;?>

                </div>


            </div>
            <nav aria-label="Page">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link text-dark" href="#" tabindex="-1">
                            Trước</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link page-active text-dark" href="#">1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link page text-dark" href="#">2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link page text-dark" href="#">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link text-dark" href="#">Sau</a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>

    <!-- Footer -->
    <footer class="footer-distributed">
        <div class="container">


            <div class="footer-left">

                <h3>OtoNamSaiGonCo's
                    <span>logo</span>
                </h3>

                <p class="footer-links">
                    <a href="#">Home</a>
                    ·
                    <a href="#">Products</a>
                    ·
                    <a href="#">About Us</a>
                    ·
                    <a href="#">Đối Tác</a>

                </p>

                <p class="footer-company-name">OtoNamSaiGonCo &copy; 2018</p>
            </div>



            <div class="footer-center">
                <!-- Button trigger modal -->
                <ul>
                    <li>
                        <a href="">Đối tác miền Bắc</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Trung</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Nam</a>
                    </li>
                </ul>
                <button type="button" class="btn modal-btn" data-toggle="modal" data-target="#exampleModal">
                    Liên hệ chúng tôi
                </button>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Liên hệ chúng tôi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="#" method="post">
                                    <input type="text" name="representator" class='partner-representator' required placeholder="Họ Tên" />
                                    <!-- <input type="text" name="company" class='partner-company' required placeholder="Tên Công Ty*" /> -->
                                    <input type="email" name="email" class='partner-email' required placeholder="Email*" />
                                    <input type="phone" name="phone" class='partner-phone' required placeholder="Số ĐT*" />
                                    <hr>
                                    <button type="button" class="btn" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn partner-registry-btn">Gửi</button> 
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-right">
                <div>
                    <i class="fa fa-map-marker"></i>
                    <p>
                        <span>Quận Thủ Đức</span> TP HCM, Việt Nam</p>
                </div>
                <div>
                    <i class="fa fa-phone"></i>
                    <p>+84 907 82 5486</p>
                </div>
                <div>
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <p>
                        <a href="mailto:support@company.com">Địa chỉ Facebook</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <div class="side-btn">
        <a href="#head">
            <img src="statics/default/img/facebook_icon.png" id="fixedbutton">
        </a>
        <a href="" class="modal-btn" data-toggle="modal" data-target="#exampleModal">
            <div class="partner-register">
                <div class="partner-register-circle text-center">
                    <i class="fas fa-handshake"></i>

                </div>
                <div class="partner-register-desc">Đăng ký đối tác</div>
            </div>
        </a>
    </div>
    <button class="back-to-top-btn text-center" id="back-to-top-btn">
        <i class="fas fa-arrow-up"></i>
    </button>

    <script src="statics/default/jquery/dist/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script src="statics/default/owl/dist/owl.carousel.min.js"></script>
    <script src="statics/default/animejs/anime.min.js"></script>
    <script src="statics/default/js/style.js"></script>

</body>

</html>