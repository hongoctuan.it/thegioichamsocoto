<section class="search-banner">

    <div class="search-banner-wrap text-center">
        <?php if (isset($error)) : ?>
            <h1 class="search-banner-title text-white text-center">
                Lỗi: <?php echo $error; ?>
            </h1>
        <?php else : ?>
            <h1 class="search-banner-title text-white text-center">
                Kết Quả Tìm Kiếm Cho:
                <br>
                "<?php echo isset($_GET['search']) ? $_GET['search'] : ""; ?>"
            </h1>
        <?php endif; ?>
    </div>
    </div>

</section>

<?php if (isset($search)) : ?>
    <?php if (isset($search['product'])) : ?>
        <section class="category-product my-3">
            <div class="container">
                <div class="product-list">
                    <div class="product-list-content">
                        <div class="row">
                            <div class="col-12">
                                <h2 class="search-section-title m-3">
                                    <span>Sản phẩm</span>
                                </h2>
                            </div>
                            <?php foreach ($search['product'] as $item) : ?>
                                <div class="col-lg-3 col-md-4 col-sm-6 category-product-item text-center my-3">
                                    <a href="<?php echo site_url('product/' . $item->slug); ?>" class="text-dark product-wrap-link">
                                        <figure class="figure">
                                            <div class="category-product-item-img">
                                                <img src="assets/public/<?php echo $item->img1 ?>" alt="" class="img-fluid">
                                            </div>
                                            <figcaption class=p-2>
                                                <h2 class="category-product-item-title"><?php echo $item->name ?></h2>
                                                <p class="category-prodcut-item-desc"><?php echo $item->short_des ?></p>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
    <?php endif; ?>

    <?php if (isset($search['category'])) : ?>
        <div class="container">
            <hr>
        </div>
        <section class="home-category my-5">
            <div class="container my-2">
                <div class="row">
                    <div class="col-12">
                        <h2 class="search-section-title m-3">
                            <span>Danh Mục Sản Phẩm</span>
                        </h2>
                    </div>
                    <?php foreach ($search['category'] as $category) : ?>
                        <div class="col-lg-3 col-md-4 col-xs-6 category-item">
                            <div class="figure category-figure-item">
                                <h3 class="category-item-title"><?php echo $category ? $category->name : ""; ?></h2>
                                    <a href="<?php echo $category ? site_url('category?cate=') . $category->id : ""; ?>" class="home-category-item-link">
                                        <img src="<?php echo $category ? site_url('assets/public/') . $category->img_large : ""; ?>" class="figure-img img-fluid rounded" width=100% height=100% alt="">
                                    </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>


    <?php endif; ?>

    <?php if (isset($search['gallery_detail'])) : ?>
        <div class="container">
            <hr>
        </div>
        <section class="gallery-main my-5">
            <div class="container my-2">
                <div class="row">
                    <div class="col-12">
                        <h2 class="search-section-title m-3">
                            <span>Hình Ảnh</span>
                        </h2>
                    </div>
                    <div class="col-12">
                        <div class="grid">
                            <div class="grid-sizer"></div>
                            <?php foreach ($search['gallery_detail'] as $image) : ?>
                                <div class="grid-item">
                                    <a href="assets/public/<?php echo $image ? $image->img : " "; ?>" data-fancybox="gallery" data-caption="<?php echo $image ? $image->description : " "; ?>">
                                        <img src="assets/public/<?php echo $image ? $image->img : " "; ?>">
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <?php endif; ?>

    <?php if (isset($search['news'])) : ?>
        <div class="container">
            <hr>
        </div>
        <section class="news-main my-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="search-section-title m-3">
                            <span>Tin tức</span>
                        </h2>
                    </div>
                    <div class="col-md-12 news-item-list">
                        <?php foreach ($search['news'] as $news_data) : ?>
                            <?php if ($news_data->parent != 0) : ?>
                                <div class="row news-item mb-5">
                                    <div class="col-md-4">
                                        <div class="news-image">
                                            <div class="news-date">
                                                <p><?php echo date('d', strtotime($news_data->created_at)); ?>
                                                    <span><?php echo date('M', strtotime($news_data->created_at)); ?></span>
                                                </p>
                                            </div>
                                            <a style="background-image:url('<?php echo site_url('assets/public/') . $news_data->img; ?>')" class="news-image-wrap"></a>
                                            <a href="<?php echo site_url('news/') . $news_data->slug; ?>" class="read-more">Đọc Thêm</a>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="news-overview text-center py-3">
                                            <h2 class="news-overview-title">
                                                <?php echo $news_data->name; ?>
                                            </h2>
                                            <p class="news-overview-author">
                                                <small><?php echo $news_data->created_by; ?></small>
                                            </p>
                                            <p class="news-overview-description"><?php echo $news_data->description; ?></p>
                                            <a href="<?php echo site_url('news/') . $news_data->slug; ?>" class="news-overview-read-more">ĐỌC THÊM</a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php if (isset($search['partner'])) : ?>
        <div class="container">
            <hr>
        </div>
        <section class="product-details mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="search-section-title m-3">
                            <span>Đối tác</span>
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <div class="tab-content text-center" id="myTabContent">
                            <div class="row shops-mien">
                                <div class="col-md-12 shops-list">
                                    <?php foreach ($search['partner'] as $item) : ?>
                                        <div class="shops-wrap shops-show-default">
                                            <h3 class="shops-title text-center mt-2"><?php echo $item->name; ?></h3>
                                            <p class="shops-address">Địa chỉ: <?php echo $item->address; ?>
                                                <br>Số ĐT: <?php echo $item->phone; ?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    <?php endif; ?>
<?php elseif (isset($error)) : ?>
    <div class="container">
        <div class="row">
            <p class="mx-auto mb-5 display-4"><b>Lỗi: Vui lòng nhập nhiều hơn 1 từ</b>
                <p>
        </div>
    </div>
<?php else : ?>

    <div class="container">
        <div class="row">
            <p class="mx-auto mb-5 display-4"><b>Không có kết quả tìm kiếm</b>
                <p>
        </div>
    </div>

<?php endif; ?>