<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="statics/default/css/index.css">
    <link rel="stylesheet" href="statics/default/css/header.css">
    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="statics/default/swiper/dist/css/swiper.min.css">
    <link
        href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Signika" rel="stylesheet">
    <title>Home</title>
</head>

<body>
    <?php include('include/V_menu.php'); ?>
    <section class="home-banner-slide">
        <div class="container-fluid">
            <div class="row">
                <div class="owl-carousel home-banner-slide-carousel">
                    <?php foreach($banners as $banner):?>
                    <div class="row justify-content-center banner-wrap">
                        <a href="<?php echo $banner?$banner->link:'';?>">
                        <img class="banner-image img-fluid" src="<?php echo $banner?$banner->img:'';?>" alt="">
                        <div class="filter"></div>
                        <!-- <div class="col-md-6 col-xs-12 text-white text-left banner-text">
                            <div class=banner-border></div>
                            <h1>SOUR GREEN APPLE ICE CREAM</h1>
                            <p>HÃY ĐẾN SIÊU THỊ X ĐỂ NHẬN ƯU ĐÃI LỚN VỚI SẢN PHẨM Y</p>
                            <a href="" class="btn btn-outline-light banner-btn">SHOP NOW</a>
                            <a href="" class="btn btn-success banner-btn">VIEW MORE</a>
                        </div> -->
                        </a>
                    </div>
                    <?php endforeach;?>
                    <!-- <div class="row justify-content-center banner-wrap">
                        <img class=banner-image src="statics/default/img/home-banner-slide.jpg" alt="">
                        <div class="filter"></div>
                        <div class="col-md-6 text-white text-left banner-text">
                            <div class=banner-border></div>
                            <h1>SOUR GREEN APPLE ICE CREAM</h1>
                            <p>HÃY ĐẾN SIÊU THỊ X ĐỂ NHẬN ƯU ĐÃI LỚN VỚI SẢN PHẨM Y</p>
                            <a href="" class="btn btn-outline-light banner-btn">SHOP NOW</a>
                            <a href="" class="btn btn-success banner-btn">VIEW MORE</a>
                        </div>
                    </div>
                    <div class="row justify-content-center banner-wrap">
                        <img class=banner-image src="statics/default/img/home-banner-slide2.jpg" alt="">
                        <div class="filter"></div>
                        <div class="col-md-6 text-white text-left banner-text">
                            <div class=banner-border></div>
                            <h1>SOUR GREEN APPLE ICE CREAM</h1>
                            <p>HÃY ĐẾN SIÊU THỊ X ĐỂ NHẬN ƯU ĐÃI LỚN VỚI SẢN PHẨM Y</p>
                            <a href="" class="btn btn-outline-light banner-btn">SHOP NOW</a>
                            <a href="" class="btn btn-success banner-btn">VIEW MORE</a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <section class="home-category my-5">
        <div class="container my-2">
            <div class="row section-title-wrap">
                <div class="section-title-line"></div>
                <h2 class="text-center section-title m-auto px-3">DANH MỤC SẢN PHẨM</h2>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-6 category-item">
                    <div class="figure category-figure-item">
                        <h3 class="category-item-title">Hoá Phẩm</h2>
                            <a href="category.html" class="home-category-item-link">

                                <!-- <figcaption class="figure-caption">Three large packs of fruit tea at the price of one.</figcaption> -->
                                <img src="statics/default/img/banner_home-2.jpg" class="figure-img img-fluid rounded"
                                    alt="">
                            </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 category-item">
                    <div class="figure category-figure-item">
                        <h3 class="category-item-title">Mỹ Phẩm</h2>
                            <!-- <p class="category-item-total">42 Sản Phẩm</p> -->
                            <a href="category.html" class="home-category-item-link">

                                <!-- <figcaption class="figure-caption">Three large packs of fruit tea at the price of one.</figcaption> -->
                                <img src="statics/default/img/category_product.png" class="figure-img img-fluid rounded"
                                    alt="">
                            </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 category-item">
                    <div class="figure category-figure-item">
                        <h3 class="category-item-title">Đồ Gia Dụng</h2>
                            <a href="category.html" class="home-category-item-link">

                                <!-- <figcaption class="figure-caption">Three large packs of fruit tea at the price of one.</figcaption> -->
                                <img src="statics/default/img/category3.jpg" class="figure-img img-fluid rounded"
                                    alt="">
                            </a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 category-item">
                    <div class="figure category-figure-item">
                        <h3 class="category-item-title">Thực Phẩm</h2>
                            <a href="category.html" class="home-category-item-link">

                                <!-- <figcaption class="figure-caption">Three large packs of fruit tea at the price of one.</figcaption> -->
                                <img src="statics/default/img/banner2.png" class="figure-img img-fluid rounded" alt="">
                            </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home-partner my-5" style="margin:0px !important">
        <div class="container">
            <!-- <div class="row partner-background"> -->
            <div class="row">
                <div class="col-lg-12 col-md-12 offset-lg-12 offset-md-12" style="padding:0px;">
                    <div class="partner-text-wrap text-white">
                        <!-- <h2 class=partner-title>Hệ Thống Phân Phối</h2> -->
                        <!-- <p class=partner-description>Sản phẩm chúng tôi đã có mặt ở toàn quốc với hệ thống đại lý:</p> -->
                        <div class="row hover01 column">
                            <div class="col-md-3 partner-item">
                                <figure>
                                    <img src="statics/default/img/icon_1.png" />
                                </figure>
                                <!-- <img class="img_line"  src="statics/default/img/icon_1.png" class="img-fluid" /> -->
                                <p class="partner-text-place">3 Miền</p>
                            </div>
                            <div class="col-md-3 partner-item">
                                <figure>
                                    <img src="statics/default/img/icon_2.png" />
                                </figure>
                                <!-- <img class="img_line"  src="statics/default/img/icon_1.png" class="img-fluid" /> -->
                                <p class="partner-text-place">10 Tỉnh Thành</p>
                            </div>
                            <div class="col-md-3 partner-item">
                                <figure>
                                    <img src="statics/default/img/icon_3.png" />
                                </figure>
                                <!-- <img class="img_line"  src="statics/default/img/icon_1.png" class="img-fluid" /> -->
                                <p class="partner-text-place">120 Đại Lý</p>
                            </div>
                            <div class="col-md-3 partner-item">
                                <figure>
                                    <img src="statics/default/img/icon_4.png" />
                                </figure>
                                <!-- <img class="img_line"  src="statics/default/img/icon_1.png" class="img-fluid" /> -->
                                <p class="partner-text-place">Hơn 3000 sản phẩm</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home-feature" style="padding-top:40px">
        <div class="container">
            <div class="row section-title-wrap">
                <div class="section-title-line"></div>
                <h2 class="text-center section-title m-auto px-3"> SẢN PHẨM NỔI BẬT</h2>
            </div>
        </div>

        <p class="text-center section-description">Dòng mô tả</p>
        <div class="container text-center">
            <div class="row text-center" style="margin-top:10px">
                <?php foreach($products as $product):?>
                <div class="col-lg-3 col-sm-6" style="padding:5px">
                    <div class="product_item">
                        <div class="hover01 hover15">

                            <figure class="product_img">
                                <img src="assets/public/<?php echo $product->img1 ?>" />
                            </figure>
                            <h6 class="product_name"><?php echo $product->name;?></h6>

                        </div>
                    </div>
                </div>
                <?php endforeach;?>

            </div>
        </div>
    </section>

    <section class="home-feature bg-light" style="padding-top:40px">
        <div class="container">
            <div class="row section-title-wrap">
                <div class="section-title-line"></div>
                <h2 class="text-center section-title m-auto home-feature-title px-3">ĐIỂM NỔI BẬT CỦA SẢN PHẨM</h2>
            </div>
        </div>

        <p class="text-center section-description">Dòng mô tả</p>
        <div class="container">
            <div class="row">

                <div class="col-lg-5 feature-item-wrap">
                    <div class="feature-item">
                        <h3 class="feature-item-title">Thân Thiện Với Môi Trường</h3>
                        <p class="feature-item-description">All the lorem ipsum generators on the Internet tend to
                            repeat predefined chunks as necessary.</p>
                    </div>
                    <div class="feature-item">
                        <h3 class="feature-item-title">Hiệu Quả Cao</h3>
                        <p class="feature-item-description">Consectetur, from a Lorem Ipsum passage, and going through
                            the cites of the word in classical.</p>
                    </div>
                    <div class="feature-item">
                        <h3 class="feature-item-title">Ít Độc Hại</h3>
                        <p class="feature-item-description">All the lorem ipsum generators on the Internet tend to
                            repeat predefined chunks as necessary.</p>
                    </div>
                </div>

                <div class="col-lg-2 hover15 column" style="margin:0px;">
                    <figure>
                        <img src="statics/default/img/product4.png" alt="feature" width=100% />
                    </figure>
                </div>

                <div class="col-lg-5 feature-item-wrap text-right">
                    <div class="feature-item">
                        <h3 class="feature-item-title">Thân Thiện Với Môi Trường</h3>
                        <p class="feature-item-description">All the lorem ipsum generators on the Internet tend to
                            repeat predefined chunks as necessary.</p>
                    </div>
                    <div class="feature-item">
                        <h3 class="feature-item-title">Hiệu Quả Cao</h3>
                        <p class="feature-item-description">Consectetur, from a Lorem Ipsum passage, and going through
                            the cites of the word in classical.</p>
                    </div>
                    <div class="feature-item">
                        <h3 class="feature-item-title">Ít Độc Hại</h3>
                        <p class="feature-item-description">All the lorem ipsum generators on the Internet tend to
                            repeat predefined chunks as necessary.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home-featured-products py-5">
        <div class="container">
            <div class="row section-title-wrap">
                <div class="section-title-line"></div>
                <h2 class="text-center section-title m-auto px-3">SẢN PHẨM NỔI BẬT</h2>
            </div>
        </div>
        <div class="container">
            <div class="owl-carousel featured-products-slide">
                <?php foreach($products as $product):?>
                <div class="justify-content-center">
                    <figure class="figure featured-products-item-figure bg-light">
                        <a href="<?php echo site_url('product?id='.$product->id);?>" class="featured-products-link">
                            <div class="featured-products-item-wrap">
                                <img class="featured-products-img img-fluid"
                                    src="assets/public/<?php echo $product->img1 ?>" alt="">
                                <figcaption class="figure-caption m-auto">
                                    <h3 class="featured-products-title text-center text-dark mt-2">
                                        <?php echo $product->name?></h3>
                                </figcaption>
                            </div>
                        </a>
                    </figure>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="footer-distributed bg-light">
        <div class="container">


            <div class="footer-left">

                <h3>OtoNamSaiGonCo's
                    <span>logo</span>
                </h3>

                <p class="footer-links">
                    <a href="#">Home</a>
                    ·
                    <a href="#">Products</a>
                    ·
                    <a href="#">About Us</a>
                    ·
                    <a href="#">Đối Tác</a>

                </p>

                <p class="footer-company-name">OtoNamSaiGonCo &copy; 2018</p>
            </div>



            <div class="footer-center">
                <!-- Button trigger modal -->
                <ul>
                    <li>
                        <a href="">Đối tác miền Bắc</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Trung</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Nam</a>
                    </li>
                </ul>
                <button type="button" class="btn modal-btn" data-toggle="modal" data-target="#exampleModal">
                    Liên hệ chúng tôi
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Liên hệ chúng tôi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="#" method="post">
                                    <input type="text" name="representator" class='partner-representator' required placeholder="Họ Tên" />
                                    <!-- <input type="text" name="company" class='partner-company' required placeholder="Tên Công Ty*" /> -->
                                    <input type="email" name="email" class='partner-email' required placeholder="Email*" />
                                    <input type="phone" name="phone" class='partner-phone' required placeholder="Số ĐT*" />
                                    <hr>
                                    <button type="button" class="btn" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn partner-registry-btn">Gửi</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-right">
                <div>
                    <i class="fa fa-map-marker"></i>
                    <p>
                        <span>Quận Thủ Đức</span> TP HCM, Việt Nam</p>
                </div>
                <div>
                    <i class="fa fa-phone"></i>
                    <p>+84 907 82 5486</p>
                </div>
                <div>
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <p>
                        <a href="mailto:support@company.com">Địa chỉ Facebook</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <div class="side-btn">
        <a href="#head">
            <img src="statics/default/img/facebook_icon.png" id="fixedbutton">
        </a>
        <a href="" class="modal-btn" data-toggle="modal" data-target="#exampleModal">
            <div class="partner-register">
                <div class="partner-register-circle text-center">
                    <i class="fas fa-handshake"></i>
                    </i>
                </div>
                <div class="partner-register-desc">Đăng ký đối tác</div>
            </div>
        </a>
    </div>



    <div class="back-to-top-btn text-center" id="back-to-top-btn">
        <i class="fas fa-arrow-up"></i>
    </div>

    <script src="statics/default/jquery/dist/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script src="statics/default/owl/dist/owl.carousel.min.js"></script>
    <script src="statics/default/swiper/dist/js/swiper.min.js"></script>
    <?php $this->load->view('default/script');?>
</body>

</html>