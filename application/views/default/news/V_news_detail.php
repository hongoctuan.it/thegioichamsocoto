
    <section class="news-content-banner">

        <div class="news-content-banner-wrap text-center" style="background-image: url('<?php echo site_url('assets/public/').$cate->img;?>">

            <h1 class="news-content-banner-title text-white text-center">
            <?php echo isset($cate)?$cate->name:"";?>
            </h1>
        </div>
        </div>
    </section>

    <section class="news-content-content my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3 text-center text-md-left">
                    <div class="news-categories">
                        <h2 class="news-categories-title">CATEGORIES</h2>
                        <ul class="news-categories-list">
                        <?php foreach($news_categories as $news_category):?>
                            <li class="news-categories-list-item <?php echo $news_category->id==$cate->id?"active":'';?>">
                                <a href="<?php echo site_url('news/category/').$news_category->slug;?>" class=text-muted><?php echo $news_category?$news_category->name:'';?></a>
                            </li>
                        <?php endforeach;?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 text-center">
                    <div class="news-content-categories">
                        <a href="<?php echo site_url('news/category/').$news_data->category['slug'];?>" class="news-content-categories-item">
                            <?php echo $news_data->category['name'];?>
                        </a>
                    </div>
                    <h2 class="news-content-title">
                        <?php echo $news_data->name;?>
                    </h2>
                    <p class="news-content-author">
                        <?php echo $news_data->created_by;?>
                    </p>
                    <div class="news-content-image text-left">
                        <div class="news-content-date">
                            <p><?php echo date('d',strtotime($news_data->created_at));?>
                                <span><?php echo date('M',strtotime($news_data->created_at));?></span>
                            </p>
                        </div>
                        <img src="<?php echo site_url('assets/public/').$news_data->img;?>" class="img-fluid" alt="">
                    </div>
                    <div class="news-content text-left py-3">
                        <p class="news-content-data"><?php echo $news_data->content;?>
                    </div>
                    <div class="fb-share-button" data-href="<?php echo site_url().$this->uri->uri_string();?>" data-layout="button" data-size="large"></div>
                </div>
            </div>
    </section>
