<section class="gallery-banner">

    <div class="gallery-banner-wrap text-center">

        <h1 class="gallery-banner-title text-white text-center">
            Thư Viện Hình Ảnh
        </h1>
    </div>

</section>
<section class="gallery-main mb-5">
    <input type="hidden" value="1" class="current-page">
    <input type="hidden" value="0" class="stopped">
    <?php if (isset($album)) : ?>
    <div class="container text-center album-wrap my-5">
        <div class="row album-title-wrap">
            <div class="album-title-line"></div>
            <h2 class="text-center album-title m-auto px-3">
                <?php echo $gallery_data ? $gallery_data->name : ""; ?>
            </h2>

        </div>
        <div class="fb-share-button" data-href="<?php echo site_url() . $this->uri->uri_string(); ?>" data-layout="button" data-size="small"></div>
        <div class="grid">
            <div class="grid-sizer"></div>
            <?php foreach ($images as $image) : ?>
            <div class="grid-item">
                <a href="<?php echo base_url('assets/avatar/'); ?><?php echo $image ? $image->img : ""; ?>" data-fancybox="<?php echo $gallery_data ? $gallery_data->id : ""; ?>" data-caption="<?php echo $image ? $image->description : " "; ?>">
                    <img src="<?php echo base_url('assets/avatar/'); ?><?php echo $image ? $image->img : " "; ?>">
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-auto">
            <a class="btn btn-lg btn-warning text-white m-1 mt-3 gallery-see-more">Xem Thêm</a>
        </div>
    </div>

    <?php else : ?>
        <?php foreach ($gallery_datas as $gallery_data) : ?>
            <?php if(!empty($gallery_data->images)):?>
            <div class="container text-center album-wrap my-5">
                <div class="row album-title-wrap">
                    <div class="album-title-line"></div>
                    <h2 class="text-center album-title m-auto px-3">
                        <?php echo $gallery_data ? $gallery_data->name : ""; ?>
                    </h2>
                </div>

                <div class="grid">
                    <div class="grid-sizer"></div>
                    <?php foreach ($gallery_data->images as $image) : ?>
                    <div class="grid-item">
                        <a href="assets/avatar/<?php echo $image ? $image->img : " "; ?>" data-fancybox="<?php echo $gallery_data ? $gallery_data->id : "
                                "; ?>" data-caption="<?php echo $image ? $image->description : " "; ?>">
                            <img src="assets/avatar/<?php echo $image ? $image->img : " "; ?>">
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="row justify-content-end">
                    <div class="col-auto">
                        <a href="<?php echo site_url("gallery/") . $gallery_data->slug; ?>" class="btn btn btn-warning text-white m-1 mt-3">Xem Thêm</a><button class="share-fb btn ml-2" style="margin-top:13px;z-index:10;background:#3b5998;font-size:24px;color:white;padding:0px 12px ;line-height:38px;height:38px"><i class="fab fa-facebook"></i> <span style="font-size:18px;margin:0px;font-weight:500;margin-bottom:2px;margin-left:4px"> Share</span></button>
                    </div>
                </div>
            </div>
            <?php else:?>
            <div class="container text-center album-wrap my-5">
                <div class="row album-title-wrap">
                    <div class="album-title-line"></div>
                    <h2 class="text-center album-title m-auto px-3">
                        <?php echo $gallery_data ? $gallery_data->name : ""; ?>
                    </h2>
                </div>
                <p>Chưa có hình ảnh</p>
            </div>

            <?php endif;?>
        <?php endforeach; ?>
    <?php endif; ?>

</section>