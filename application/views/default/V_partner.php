<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="statics/default/css/header.css">
    <link rel="stylesheet" href="statics/default/css/shop.css">
    <link rel="stylesheet" href="statics/default/css/category.css">

    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="statics/default/swiper/dist/css/swiper.min.css">
    <link rel="stylesheet" href="statics/default/perfect-scrollbar/css/perfect-scrollbar.css">
    <link
        href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese"
        rel="stylesheet">
    <title>Find a Store</title>
</head>

<body>
    <?php include('include/V_menu.php'); ?>
    <section class="shops-banner">

        <div class="shops-banner-wrap text-center">
            <h1 class="shops-banner-title text-white text-center">
                Đối tác của chúng tôi
            </h1>
        </div>
        </div>

    </section>
    <section class="product-details mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-1">
                    <ul class="nav nav-tabs text-center justify-content-center bg-light stores-menu m-auto" id="myTab"
                        role="tablist">
                        <li class="nav-item mx-3 my-2">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#product-details-desc"
                                role="tab" aria-controls="product-details-desc" aria-selected="true">Đối tác miền
                                Bắc</a>
                        </li>
                        <li class="nav-item mx-3 my-2">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#product-details-more-info"
                                role="tab" aria-controls="product-details-more-info" aria-selected="false">Đối tác miền
                                Trung</a>
                        </li>
                        <li class="nav-item mx-3 my-2">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#product-details-brand-info"
                                role="tab" aria-controls="product-details-brand-info" aria-selected="false">Đối tác miền
                                Nam</a>
                        </li>

                    </ul>
                </div>

                <div class="col-md-12">
                    <div class="tab-content text-center" id="myTabContent">
                        <div class="tab-pane fade show active" id="product-details-desc" role="tabpanel"
                            aria-labelledby="product-details-desc-tab">
                            <div class="row shops-mien">
                                <div class="col-md-3 shops-list">
                                    <div class="shops-list-title-section">
                                        <h3 class=shops-list-title>Miền Bắc</h3>
                                    </div>
                                    <?php foreach($partner_bac as $item):?>
                                        <div class="shops-wrap shops-show-default">
                                            <h3 class="shops-title text-center mt-2"><?php echo $item->name;?></h3>
                                            <p class="shops-address">Địa chỉ: <?php echo $item->address;?>
                                                <br>Số ĐT: <?php echo $item->phone;?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="col-md-9">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62694.93338363085!2d106.71814491147437!3d10.854608470658878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d85e042bf04b%3A0xbb26baec1664394d!2zVGjhu6cgxJDhu6ljLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1555423397586!5m2!1svi!2s"
                                        width="100%" height="600" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="product-details-more-info" role="tabpanel"
                            aria-labelledby="product-details-more-info-tab">
                            <div class="row shops-mien">
                                <div class="col-md-3 shops-list">
                                    <div class="shops-list-title-section">
                                        <h3 class=shops-list-title>Miền Trung</h3>
                                    </div>
                                    <?php foreach($partner_trung as $item):?>
                                        <div class="shops-wrap shops-show-default">
                                            <h3 class="shops-title text-center mt-2"><?php echo $item->name;?></h3>
                                            <p class="shops-address">Địa chỉ: <?php echo $item->address;?>
                                                <br>Số ĐT: <?php echo $item->phone;?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="col-md-9">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62694.93338363085!2d106.71814491147437!3d10.854608470658878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d85e042bf04b%3A0xbb26baec1664394d!2zVGjhu6cgxJDhu6ljLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1555423397586!5m2!1svi!2s"
                                        width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="product-details-brand-info" role="tabpanel"
                            aria-labelledby="product-details-brand-info-tab">
                            <div class="row shops-mien">
                                <div class="col-md-3 shops-list">
                                    <div class="shops-list-title-section">
                                        <h3 class=shops-list-title>Miền Nam</h3>
                                    </div>
                                    <?php foreach($partner_nam as $item):?>
                                        <div class="shops-wrap shops-show-default">
                                            <h3 class="shops-title text-center mt-2"><?php echo $item->name;?></h3>
                                            <p class="shops-address">Địa chỉ: <?php echo $item->address;?>
                                                <br>Số ĐT: <?php echo $item->phone;?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="col-md-9">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62694.93338363085!2d106.71814491147437!3d10.854608470658878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d85e042bf04b%3A0xbb26baec1664394d!2zVGjhu6cgxJDhu6ljLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1555423397586!5m2!1svi!2s"
                                        width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Footer -->
    <footer class="footer-distributed">
        <div class="container">


            <div class="footer-left">

                <h3>OtoNamSaiGonCo's
                    <span>logo</span>
                </h3>

                <p class="footer-links">
                    <a href="#">Home</a>
                    ·
                    <a href="#">Products</a>
                    ·
                    <a href="#">About Us</a>
                    ·
                    <a href="#">Đối Tác</a>

                </p>

                <p class="footer-company-name">OtoNamSaiGonCo &copy; 2018</p>
            </div>



            <div class="footer-center">
                <!-- Button trigger modal -->
                <ul>
                    <li>
                        <a href="">Đối tác miền Bắc</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Trung</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Nam</a>
                    </li>
                </ul>
                <button type="button" class="btn modal-btn" data-toggle="modal" data-target="#exampleModal">
                    Liên hệ chúng tôi
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Liên hệ chúng tôi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="#" method="post">
                                    <input type="text" name="representator" class='partner-representator' required placeholder="Họ Tên" />
                                    <!-- <input type="text" name="company" class='partner-company' required placeholder="Tên Công Ty*" /> -->
                                    <input type="email" name="email" class='partner-email' required placeholder="Email*" />
                                    <input type="phone" name="phone" class='partner-phone' required placeholder="Số ĐT*" />
                                    <hr>
                                    <button type="button" class="btn" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn partner-registry-btn">Gửi</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-right">
                <div>
                    <i class="fa fa-map-marker"></i>
                    <p>
                        <span>Quận Thủ Đức</span> TP HCM, Việt Nam</p>
                </div>
                <div>
                    <i class="fa fa-phone"></i>
                    <p>+84 907 82 5486</p>
                </div>
                <div>
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <p>
                        <a href="mailto:support@company.com">Địa chỉ: Facebook</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>


    </footer>
    <div class="side-btn">
        <a href="#head">
            <img src="statics/default/img/facebook_icon.png" id="fixedbutton">
        </a>
        <a href="" class="modal-btn" data-toggle="modal" data-target="#exampleModal">
            <div class="partner-register">
                <div class="partner-register-circle text-center">
                    <i class="fas fa-handshake"></i>
                    </i>
                </div>
                <div class="partner-register-desc">Đăng ký đối tác</div>
            </div>
        </a>
    </div>
    <button class="back-to-top-btn text-center" id="back-to-top-btn">
        <i class="fas fa-arrow-up"></i>
    </button>
    <!-- Footer -->
    <script src="statics/default/jquery/dist/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script src="statics/default/owl/dist/owl.carousel.min.js"></script>
    <script src="statics/default/animejs/anime.min.js"></script>
    <script src="statics/default/swiper/dist/js/swiper.min.js"></script>
    <script src="statics/default/perfect-scrollbar/dist/perfect-scrollbar.js"></script>
    <script src="statics/default/js/style.js"></script>

</body>

</html>