<section class="category-banner">
    <div class="category-banner-wrap text-center">
        <div class="category-banner-wrap-layer"></div>
        <!-- <h1 class="category-title dropdown-btn text-white text-center mx-auto">
        <?php echo isset($category_id)?$cats[$category_id]->name:'Tất cả dịch vụ' ?>
        </h1> -->
    </div>
</section>

<section class="category-product my-3">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="product-list">
                    <input type="hidden" value="1" class="current-page">
                    <input type="hidden" value="0" class="stopped">
                    <div class="row">
                        <div class="col-lg-3">
                                <h3 class="category-sidebar-title">Dịch Vụ</h3>
                                <ul class="sub-category-list level1">
                                <?php foreach($cats as $cat): ?>
                                        <li class="sub-category-list-item level1 <?php echo isset($category_id)&&$category_id==$cat->id?"active":'';?>">
                                            <h3 class="title level1-title"><a href="<?php echo site_url("category/").$cat->slug;?><?php echo $style?"?style=".$style:"";?>"><?php echo $cat->name;?></a>
                                            </h3>
                                        </li>
                                <?php endforeach;?>
                                </ul>
                        </div>
                        <div class="col-lg-9">
                            <div class="row text-secondary bread-crumb">
                                <div class="col-6 mb-3 text-left">
                                    <a href="<?php echo site_url('category');?>" class="text-secondary <?php echo empty($category_id)?"text-active":"";?>">Home</a> 
                                    <?php if(isset($category_id)):?>
                                        <?php foreach($category_tree as $level0): ?>
                                            <?php if($category_id==$level0->id):?>
                                            / <b class="text-active"><a href="<?php echo site_url('category/').$level0->slug."?style=".$style;?>" class=text-secondary><?php echo $level0->name;?></a></b>
                                            <?php elseif(isset($level0->level1)): ?>
                                                <?php foreach($level0->level1 as $level1):?>
                                                    <?php if($category_id==$level1->id):?>
                                                    / <a href="<?php echo site_url('category/').$level0->slug."?style=".$style;?>" class=text-secondary><?php echo $level0->name;?></a> / <b class="text-active"><a href="<?php echo site_url('category/').$level1->slug;?>" class=text-secondary><?php echo $level1->name;?></a></b> 
                                                    <?php elseif(isset($level1->level2)): ?> 
                                                        <?php foreach($level1->level2 as $level2):?>
                                                            <?php if($category_id==$level2->id):?>
                                                            / <a href="<?php echo site_url('category/').$level0->slug."?style=".$style;?>" class=text-secondary><?php echo $level0->name;?></a> / <a href="<?php echo site_url('category/').$level1->slug;?>" class=text-secondary><?php echo $level1->name;?></a> / <b class="text-active"><a href="<?php echo site_url('category/').$level2->slug;?>" class=text-secondary><?php echo $level2->name;?></a></b>
                                                            <?php endif;?>
                                                        <?php endforeach;?>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </div>
                                <div class="col-6 text-right">
                                    <?php if($order=="name"):?>
                                        <?php if($by=="asc"):?>
                                            <?php
                                            $query = $_GET;
                                            // replace parameter(s)
                                            $query['by'] = 'desc';
                                            $query['order'] = 'name';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">TÊN <span style="color:orange;"><i class="fas fa-caret-up"></i></span></a>
                                            <?php
                                            $query['by'] = 'asc';
                                            $query['order'] = 'id';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">MỚI NHẤT <i class="fas fa-caret-up"></i></i></a>
                                        <?php elseif($by=="desc"):?>
                                            <?php $query = $_GET;
                                            // replace parameter(s)
                                            $query['by'] = 'asc';
                                            $query['order'] = 'name';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">TÊN <span style="color:orange;"><i class="fas fa-caret-down"></i></i></span></a>
                                            <?php
                                            $query['by'] = 'asc';
                                            $query['order'] = 'id';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">MỚI NHẤT <i class="fas fa-caret-up"></i></i></a>
                                        <?php endif;?>
                                    <?php elseif($order=="id"):?>
                                        <?php if($by=="asc"):?>
                                            <?php
                                            $query = $_GET;
                                            // replace parameter(s)
                                            $query['by'] = 'asc';
                                            $query['order'] = 'name';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">TÊN <i class="fas fa-caret-up"></i></i></a>
                                            <?php
                                            $query['by'] = 'desc';
                                            $query['order'] = 'id';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">MỚI NHẤT <span style="color:orange;"><i class="fas fa-caret-up"></i></span></a>
                                        <?php elseif($by=="desc"):?>
                                            <?php $query = $_GET;
                                            // replace parameter(s)
                                            $query['by'] = 'asc';
                                            $query['order'] = 'name';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">TÊN <i class="fas fa-caret-up"></i></i></a>
                                            <?php
                                            $query['by'] = 'asc';
                                            $query['order'] = 'id';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">MỚI NHẤT <span style="color:orange;"><i class="fas fa-caret-down"></i></i></span></a>
                                        <?php endif;?>
                                    <?php else:?>
                                        <?php
                                            $query = $_GET;
                                            // replace parameter(s)
                                            $query['by'] = 'asc';
                                            $query['order'] = 'name';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">TÊN <i class="fas fa-caret-up"></i></i></a>
                                        <?php
                                            $query['by'] = 'desc';
                                            $query['order'] = 'id';
                                            $sort = http_build_query($query);
                                            ?>
                                            <a href="<?php echo base_url().uri_string(); ?>?<?php echo $sort; ?>" style="text-dectoration:none;color:#3f3f3f;font-size:.9rem;font-weight:500" class="mx-3">MỚI NHẤT <i class="fas fa-caret-down"></i></i></a>
                                    <?php endif;?>
                                    <?php if($style=="list"):?>
                                        <?php
                                        $query = $_GET;
                                        // replace parameter(s)
                                        $query['style'] = 'grid';
                                        $grid = http_build_query($query);
                                        ?>
                                        <a href="<?php echo base_url().uri_string(); ?>?<?php echo $grid; ?>" style="text-dectoration:none;color:orange;font-size:1.2rem;" class="mx-1"><i class="fas fa-th"></i></a>
                                    <?php elseif($style=="grid"||!$style):?>
                                        <?php
                                        $query = $_GET;
                                        $query['style'] = 'list';
                                        $list = http_build_query($query);
                                        ?>
                                        <a href="<?php echo base_url().uri_string(); ?>?<?php echo $list; ?>" style="text-dectoration:none;color:orange;font-size:1.2rem;"  class="mx-1"><i class="fas fa-list"></i></a>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="row product-list-content mt-2">
                                <?php if($products):?>
                                    <?php if($style=='grid'||!$style):?>
                                        <?php foreach($products as $item): ?>
                                            <div class="col-md-4 col-sm-6 category-product-item text-center">
                                                <!-- <a href="<?php echo site_url('product?id='.$item->id);?>" -->

                                                    <figure class="figure category-product-figure-grid">
                                                    <a href="<?php echo site_url('product/'.$item->slug);?>"
                                                    class="text-dark product-wrap-link">
                                                        <div class="category-product-item-img">
                                                            <img src="<?php echo site_url();?>assets/avatar/<?php echo $item->img1 ?>" alt=""
                                                                class="img-fluid product-image">
                                                        </div>
                                                        </a>
                                                        <figcaption class="p-2 k-flex align-items-end">
                                                            <h2 class="category-product-item-title"><?php echo $item->name?></h2>
                                                            <a href="<?php echo site_url('category/').$item->category_slug;?>" class="btn" style="font-size:16px;color:orange;padding:0px 5px;line-height:24px;height:24px;right:10px;"> <span style="font-size:14px;margin:0px;font-weight:500;margin-bottom:2px;margin-left:3px"> <?php echo $item->category_name;?></span></a>
                                                            <?php if($item->price!=0){?>
                                                                <p class="" style="color:grey;line-height:24px"> <span style="font-size:14px;margin:0px;font-weight:400;margin-bottom:2px;margin-left:3px"> <?php echo number_format($item->price, 0, ',', '.');?> VND</span></p>
                                                            <?php }?>
                                                            <button class="share-fb btn" style="z-index:10;background:#3b5998;font-size:14px;color:white;padding:0px 5px ;line-height:20px;height:20px;bottom:35px;left:50%;transform:translateX(-50%)"><i class="fab fa-facebook"></i> <span style="line-height:20px;font-size:11px;margin:0px;font-weight:400;margin-bottom:2px;margin-left:3px"> Chia Sẻ</span></button>
                                                            <div class="zalo-share-button" data-href="<?php echo site_url('product/'.$item->slug);?>" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false style="position:absolute;bottom:5px;left:50%;transform:translateX(-50%)"></div>
                                                        </figcaption>
                                                    </figure>
                                            </div>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <?php foreach($products as $item): ?>
                                            <div class="category-product-item text-center my-2 col-sm-5">
                                                <!-- <a href="<?php echo site_url('product?id='.$item->id);?>" -->
                                                <a href="<?php echo site_url('product/'.$item->slug);?>"
                                                    class="text-dark product-wrap-link">
                                                    <figure class="figure category-product-figure">
                                                        <div class="category-product-item-img">
                                                            <img src="<?php echo site_url();?>assets/avatar/<?php echo $item->img1 ?>" alt=""
                                                                class="img-fluid">
                                                        </div>
                                                    </figure>
                                                </a>
                                            </div>
                                            <div class="col-sm-7 category-product-item-text border-bottom my-2">
                                                <h2 class="category-product-item-title"><?php echo $item->name?></h2>
                                                <?php if($item->price!=0){?>
                                                <p class="mb-0" style="color:orange;line-height:24px"> <span style="font-size:14px;margin:0px;font-weight:500;margin-bottom:2px;margin-left:3px"> <?php echo number_format($item->price, 0, ',', '.');?> VND</span></p>
                                                <?php }?>
                                                
                                                <a href="<?php echo site_url('category/').$item->category_slug;?>" class="btn" style="background:orange;font-size:16px;color:white;padding:0px 5px;line-height:24px;height:24px;"> <span style="font-size:14px;margin:0px;font-weight:500;margin-bottom:2px;margin-left:3px"> <?php echo $item->category_name;?></span></a>
                                                <!-- <p class="mb-0"><small class=pl-2 style="color:green;"><b>Quy Cách</b>: 12 thùng</small></p> -->
                                                <p class="category-prodcut-item-desc"><?php echo $item->short_des?></p>
                                                <div>
                                                    <div class="zalo-share-button" data-href="<?php echo site_url('product/'.$item->slug);?>" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false style="margin-top:50px;"></div>
                                                    <button class="share-fb btn" style="z-index:10;background:#3b5998;font-size:12px;color:white;padding:0px 5px ;line-height:20px;height:20px;position:relative;margin-top:29px"><i class="fab fa-facebook"></i> <span style="font-size:12px;margin:0px;font-weight:400;margin-bottom:3px;margin-left:3px"> Chia Sẻ</span></button>
                                                </div>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php else:?>
                                <p class="mx-auto mb-5"><b>Không có dịch nào</b></p>
                                <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($products):?>
                    <div class="col-12 btn load-more text-center">
                        <p>Xem Thêm Dịch Vụ</p>
                    </div>
                <?php endif;?>
                <hr>
                <!-- <div class="col-12 category-hot-products-md">
                            <h3 class="category-sidebar-title">Sản Phẩm Hot</h3>
                            <ul class="category-sidebar-hot-products">
                                <?php for($i=0;$i<3;$i++):?>
                                    <?php if(isset($hot_products[$i])):?>
                                    <li class="category-sidebar-hot-products-item">
                                        <a href="<?php echo site_url('product/'.$hot_products[$i]->slug);?>" class="text-dark product-wrap-link">
                                            <div class="row">
                                                <div class="col-4">
                                                    <img src="<?php echo base_url();?>assets/avatar/<?php echo $hot_products[$i]->img1 ?>" alt=""
                                                        class="img-fluid">
                                                </div>
                                                <div class="col-8">
                                                <h4 class="" style="font-size:.9rem;color:#3f3f3f;">
                                                    <?php echo $hot_products[$i]->name;?>
                                                </h4>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                <?php endfor;?>
                            </ul>
                        </div>
                </div> -->
            </div>
        </div>
    </div>
</section>