<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144497109-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-144497109-1');
    </script> -->
    <meta http-equiv=”content-language” content=”vi” />
    <meta http-equiv=”Content-Type” content=”text/html; charset=utf-8″ />
    <link href=”favicon.ico” rel=”shortcut icon” type=”image/x-icon” />
    <meta name='revisit-after' content='1 days' />
    <meta name=”robots” content=”all” />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ($this->uri->rsegment(1) == "product") : ?>
    <title><?php echo isset($product) ? $product->name : ""; ?></title>
    <meta name=”description” content="<?php isset($product) ? $product->short_des : "" ?>" />
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($product) ? $product->name : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($product) ? $product->short_des : ""; ?>" />
    <meta property="og:image" content="<?php echo isset($product) ? base_url('assets/avatar/') . $product->img1 : ""; ?>" />
    <meta name="image" content="<?php echo isset($product) ? base_url('assets/avatar/') . $product->img1 : ""; ?>" />
    <meta property="og:image:width" content="700">
    <meta property="og:image:height" content="630">
    <meta property="fb:app_id" content="500244867373071" />
    <?php elseif ($this->uri->rsegment(1) == "category") : ?>
    <?php if (isset($meta)) : ?>
    <title><?php echo isset($meta) ? $meta['title'] : ""; ?></title>
    <meta name="description" content="<?php echo isset($meta) ? $meta['meta'] : "" ?>" />
    <?php else : ?>
    <title><?php echo isset($cates) ? $cates[0]->name : ""; ?></title>
    <meta name="description" content="<?php echo isset($cates) ? $cates->description : "" ?>" />
    <?php endif ?>
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($product) ? $product->name : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($product) ? $product->short_des : ""; ?>" />
    <meta property="og:image" content="<?php echo isset($product) ? base_url('assets/avatar/') . $product->img1 : ""; ?>" />
    <meta property="og:image:width" content="700">
    <meta property="og:image:height" content="630">
    <meta property="fb:app_id" content="500244867373071" />
    <?php elseif ($this->uri->rsegment(1) == "about") : ?>
    <title><?php echo isset($introduce_info) ? $introduce_info->title : ""; ?></title>
    <meta name="description" content="<?php echo isset($introduce_info) ? $introduce_info->short_des : "" ?>" />
    <?php elseif ($this->uri->rsegment(1) == "news") : ?>
    <?php if (isset($news_data)) : ?>
    <title><?php echo isset($news_data) ? $news_data->name : ""; ?></title>
    <meta name="description" content="<?php echo isset($news_data) ? $news_data->description : "" ?>" />
    <?php elseif (isset($meta)) : ?>
    <title><?php echo isset($meta) ? $meta['title'] : ""; ?></title>
    <meta name="description" content="<?php echo isset($meta) ? $meta['meta'] : "" ?>" />
    <?php else : ?>
    <title><?php echo isset($cate) ? $cate->name : ""; ?></title>
    <meta name="description" content="<?php echo isset($cate) ? $cate->description : "" ?>" />
    <?php endif; ?>
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($news_data) ? $news_data->name : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($news_data) ? $news_data->description : ""; ?>" />
    <meta property="og:image" content="<?php echo isset($news_data) ? site_url('assets/news/') . $news_data->img : ""; ?>" />
    <meta property="og:image:width" content="700">
    <meta property="og:image:height" content="630">
    <meta property="fb:app_id" content="500244867373071" />
    <?php elseif ($this->uri->rsegment(1) == "gallery" && isset($album)) : ?>
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($gallery_data) ? $gallery_data->name : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($gallery_data) ? $gallery_data->description : ""; ?>" />
    <meta property="og:image" content="<?php echo isset($images)||!empty($images) ? $images[0]->img : ""; ?>" />
    <meta property="og:image:width" content="700">
    <meta property="og:image:height" content="630">
    <meta property="fb:app_id" content="500244867373071" />
    <?php elseif ($this->uri->rsegment(1) == "search") : ?>
    <title><?php echo isset($meta) ? $meta['title'] : ""; ?></title>
    <meta name="description" content="<?php echo isset($meta) ? $meta['meta'] : "" ?>" />
    <?php else : ?>
    <title><?php echo $meta ? $meta['title'] : ""; ?></title>
    <meta name="description" content="<?php echo isset($meta) ? $meta['meta'] : "" ?>" />
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo base_url();?>statics/default/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>statics/default/fontawesome-free/css/all.css">
    <?php
    switch ($this->uri->rsegment(1)) {
        case 'category':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/category.css") . '">';
            break;

        case 'about':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/about.css") . '">';
            break;

        case 'partner':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/partner.css") . '">';
            break;

        case 'product':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/product.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/partner.css") . '">';
            break;

        case 'gallery':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/gallery.css") . '">
        <link rel="stylesheet" href="' . site_url("statics/default/fancybox-master/dist/jquery.fancybox.min.css") . '">';
            break;

        case 'news':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/news.css") . '">
        <link rel="stylesheet" href="' . site_url("statics/default/css/news_detail.css") . '">';
            break;

        default:
            echo '<link rel="stylesheet" href="statics/default/css/index.css">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/category.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/partner.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/product.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/gallery.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/fancybox-master/dist/jquery.fancybox.min.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/news.css") . '">';
    }
    ?>
    <link rel="stylesheet" href="<?php echo site_url('statics/default/css/header.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/swiper/dist/css/swiper.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/perfect-scrollbar/css/perfect-scrollbar.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/owl/dist/assets/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/owl/dist/assets/owl.theme.default.min.css'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Yeseva+One&display=swap&subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese" rel="stylesheet">
    <style>
        body {
            font-family: 'Roboto', sans-serif;
        }
    </style>
    <link rel="shortcut icon" type="ico" href="<?php echo base_url('assets/public/logo.png'); ?>" />
</head>

<body>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                xfbml: true,
                version: 'v3.3'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Your customer chat code -->
    <div class="fb-customerchat" attribution=setup_tool page_id="460926988025435" theme_color="#ffc300" logged_in_greeting="Xin Chào, chúng tôi có thể giúp gì cho bạn?" logged_out_greeting="Xin Chào, chúng tôi có thể giúp gì cho bạn?">
    </div>

    <?php if ($this->uri->rsegment(1) == "product" || $this->uri->rsegment(1) == "category" || $this->uri->rsegment(1) == "news") : ?>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=500244867373071&autoLogAppEvents=1"></script>
    <?php endif; ?>
    <header>
        <nav class="navbar-expand-lg navbar-dark navbar-lg bg-white" >

            <div class="container navbar-desktop brand-bar">
            <!-- <div class="">
                <img src="<?php echo base_url('');?>assets/public/nguyenquanlogo.ico" class="img-fluid" style="max-height:70px">
            </div> -->
                <div class='ml-1'> 
                    
                    <a class="navbar-brand brand-md my-0 text-center" href="<?php echo site_url(); ?>">
                    <img src="<?php echo site_url('assets/public/logo.png')?>" width="70px" style="float:left"/>
                        <div style="padding-top:5px">Công Ty TNHH Thương Mại Dịch Vụ
                        <br>Ô Tô Nam Sài Gòn</div>
                    </a>
                </div>
                <div class="owl-carousel featured-products-slide" style="margin-left:70px;">
            <?php if ($partner) : ?>
                <?php foreach ($partner as $item) : ?>
                    <div class="justify-content-center">
                        <!-- <figure class="figure featured-products-item-figure"> -->
                                <div class="featured-products-item-wrap">
                                    <img class="slide_logo" src="assets/partner/<?php echo $item->image ?>" alt="">
                                </div>
                        <!-- </figure> -->
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
                
                <!-- <marquee width="60%" direction="left" height="100px">
                <?php foreach ($partner as $item) : ?>
                    <img class="" src="assets/partner/<?php echo $item->image ?>" alt="" width="60px">
                <?php endforeach; ?>
                    </marquee> -->
                <!-- <form class="form-inline ml-auto my-lg-0" method=get action="<?php echo site_url("/search"); ?>">
                    <input class="form-control mr-sm-2 search-input" type="search" name="search" placeholder="Từ khoá tìm kiếm">
                    <button type="submit" class="search-submit-button text-dark">
                        <i class="fas fa-search"></i>
                    </button>
                </form> -->
            </div>

        </nav>
        <nav class="navbar-expand-lg navbar-dark navbar-lg" id="navbartop">
            <div class="container menu-bar">
                <ul class="navbar-nav navbar-nav-lg">
                    <li class="nav-item <?php echo $this->uri->segment(1) == '' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>">Trang Chủ
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'category' || $this->uri->segment(1) == 'product' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>category">Dịch vụ</a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'gallery' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>gallery">Hình Ảnh</a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'news' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>news">Tin Tức</a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'about' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>about">Về Chúng Tôi</a>
                    </li>
                    <!-- <li class="nav-item <?php echo $this->uri->segment(1) == 'partner' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>partner">Đối Tác</a>
                    </li> -->

                </ul>
            </div>
    </nav>
                <nav class="navbar navbar-expand-lg navbar-dark navbar-mobile">
                    <div class="container">
                    <a class="navbar-brand brand-md my-0 text-center" href="<?php echo site_url(); ?>">
                    <img src="<?php echo site_url('assets/public/logo.png')?>" width="70px" style="float:left;position: relative;"/>
                        <div style="color:white; position:relative; font-size:10px; float:left; top:20px; margin-left:10px">Công Ty TNHH Thương Mại Dịch Vụ
                        <br>Ô Tô Nam Sài Gòn</div>
                    </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item <?php echo $this->uri->segment(1) == '' ? "active" : ""; ?>">
                                    <a class="nav-link text-white" href="<?php echo site_url(); ?>">Trang Chủ
                                    </a>
                                </li>
                                <li class="nav-item <?php echo $this->uri->segment(1) == 'category' || $this->uri->segment(1) == 'product' ? "active" : ""; ?>">
                                    <a class="nav-link text-white" href="<?php echo site_url(); ?>category">Dịch vụ</a>
                                </li>
                                <li class="nav-item <?php echo $this->uri->segment(1) == 'gallery' ? "active" : ""; ?>">
                                    <a class="nav-link text-white" href="<?php echo site_url(); ?>gallery">Hình Ảnh</a>
                                </li>
                                <li class="nav-item <?php echo $this->uri->segment(1) == 'news' ? "active" : ""; ?>">
                                    <a class="nav-link text-white" href="<?php echo site_url(); ?>news">Tin Tức</a>
                                </li>
                                <li class="nav-item <?php echo $this->uri->segment(1) == 'about' ? "active" : ""; ?>">
                                    <a class="nav-link text-white" href="<?php echo site_url(); ?>about">Về Chúng Tôi</a>
                                </li>
                            </ul>

                            <!-- <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2 search-input" type="search" placeholder="Từ khoá tìm kiếm">
                                <button type="submit" class="search-submit-button">
                                    <i class="fas fa-search"></i>
                                </button>
                            </form> -->
                        </div>
                    </div>
                </nav>
    </header>