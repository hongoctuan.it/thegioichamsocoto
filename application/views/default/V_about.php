<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="stylesheet" href="statics/default/css/about.css">
    <link rel="stylesheet" href="statics/default/css/header.css">
    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese"
        rel="stylesheet">
    <title>About Us</title>
</head>

<body>
    <?php include('include/V_menu.php'); ?>
    <section class="about-banner">

        <div class="about-banner-wrap text-center">

            <h1 class="about-banner-title text-white text-center">
                <?php echo $introduce_info->slogan ?>
            </h1>
            <div class="about-banner-text-wrap">
                <p class="about-banner-text text-white">
                    <?php echo $introduce_info->short_des ?>
                
                </p>
            </div>
        </div>
        </div>

    </section>

    <section class="about-introduction">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="about-section-title m-4">
                        <span>Giới Thiệu</span> Công Ty
                    </h2>
                </div>
                <div class="col-12">
                    <?php echo $introduce_info->detail_des;?>
                </div>
        
            </div>
        </div>
    </section>

    <section class="about-history">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="about-section-title m-4 text-center">
                        <span>Lịch Sử</span> Công Ty
                    </h2>
                </div>
            </div>
            <div class="container">
                <?php foreach($history as $item):?>
                    <div class="timeline-item" date-is='<?php echo $item->time ?>'>
                        <h3><?php echo $item->title ?></h3>
                        <div class="row">
                            <div class="col-md-9">
                                <p><?php echo $item->des ?></p>
                            </div>
                            <div class="col-md-3">
                                <img src="<?php echo site_url('assets/public/'.$item->image_01) ?>" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
    </section>


    <!-- Footer -->
    <footer class="footer-distributed">
        <div class="container">


            <div class="footer-left">

                <h3>OtoNamSaiGonCo's
                    <span>logo</span>
                </h3>

                <p class="footer-links">
                    <a href="#">Home</a>
                    ·
                    <a href="#">Products</a>
                    ·
                    <a href="#">About Us</a>
                    ·
                    <a href="#">Đối Tác</a>

                </p>

                <p class="footer-company-name">OtoNamSaiGonCo &copy; 2018</p>
            </div>



            <div class="footer-center">
                <!-- Button trigger modal -->
                <ul>
                    <li>
                        <a href="">Đối tác miền Bắc</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Trung</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Nam</a>
                    </li>
                </ul>
                <button type="button" class="btn modal-btn" data-toggle="modal" data-target="#exampleModal">
                    Liên hệ chúng tôi
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Liên hệ chúng tôi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="#" method="post">
                                    <input type="text" name="representator" class='partner-representator' required placeholder="Họ Tên" />
                                    <!-- <input type="text" name="company" class='partner-company' required placeholder="Tên Công Ty*" /> -->
                                    <input type="email" name="email" class='partner-email' required placeholder="Email*" />
                                    <input type="phone" name="phone" class='partner-phone' required placeholder="Số ĐT*" />
                                    <hr>
                                    <button type="button" class="btn" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn partner-registry-btn">Gửi</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-right">
                <div>
                    <i class="fa fa-map-marker"></i>
                    <p>
                        <span>Quận Thủ Đức</span> TP HCM, Việt Nam</p>
                </div>
                <div>
                    <i class="fa fa-phone"></i>
                    <p>+84 907 82 5486</p>
                </div>
                <div>
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <p>
                        <a href="mailto:support@company.com">Địa chỉ Facebook</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Social buttons -->

    <!-- Copyright -->
    <!-- <div class="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="index.html" class=text-dark> OtoNamSaiGonCo</a>
        </div> -->
    <!-- Copyright -->

    </footer>
    <div class="side-btn">
        <a href="#head">
            <img src="statics/default/img/facebook_icon.png" id="fixedbutton">
        </a>
        <a href="" class="modal-btn" data-toggle="modal" data-target="#exampleModal">
            <div class="partner-register">
                <div class="partner-register-circle text-center">
                    <i class="fas fa-handshake"></i>
                    </i>
                </div>
                <div class="partner-register-desc">Đăng ký đối tác</div>
            </div>
        </a>
    </div>
    <button class="back-to-top-btn text-center" id="back-to-top-btn">
        <i class="fas fa-arrow-up"></i>
    </button>

    <script src="statics/default/jquery/dist/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script src="statics/default/owl/dist/owl.carousel.min.js"></script>
    <script src="statics/default/swiper/dist/js/swiper.min.js"></script>
    <?php $this->load->view('default/script');?>

</body>

</html>