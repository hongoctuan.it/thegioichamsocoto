
    <section class="about-banner">

        <div class="about-banner-wrap text-center" style="background-image: url('<?php echo site_url('assets/avatar/'.$introduce_info->img)?>')">

            <h1 class="about-banner-title text-white text-center">
                <?php echo $introduce_info->slogan ?>
            </h1>
            <div class="about-banner-text-wrap">
                <p class="about-banner-text text-white" >
                    <?php echo $introduce_info->short_des ?>
                
                </p>
            </div>
        </div>
        </div>

    </section>

    <section class="about-introduction">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="about-section-title m-4">
                        <span>Giới Thiệu</span> Công Ty
                    </h2>
                </div>
                <div class="col-12">
                    <?php echo $introduce_info->detail_des;?>
                </div>
        
            </div>
        </div>
    </section>

    <section class="about-history">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="about-section-title m-4 text-center">
                        <span>Thông Tin</span> Công Ty
                    </h2>
                </div>
            </div>




            <div class="container">
                <?php foreach($history as $item):?>
                    <div class="timeline-item" date-is='<?php echo $item->time ?>'>
                        <h3><?php echo $item->title ?></h3>
                        <div class="row">
                            <div class="col-md-9">
                                <?php echo $item->des?>
                            </div>
                            <div class="col-md-3">
                                <img src="<?php echo site_url('assets/avatar/'.$item->img) ?>" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
    </section>
