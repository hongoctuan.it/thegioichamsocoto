<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="stylesheet" href="statics/default/css/product.css">
    <link rel="stylesheet" href="statics/default/css/category.css">
    <link rel="stylesheet" href="statics/default/css/header.css">
    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="statics/default/owl/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="statics/default/swiper/dist/css/swiper.min.css">
    <link rel="stylesheet" href="statics/default/perfect-scrollbar/css/perfect-scrollbar.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese"
        rel="stylesheet">
    <title>Product Detail</title>
</head>

<body>
    <?php include('include/V_menu.php'); ?>
    <section class="product-overview py-4">
        <div class="container">
            <div class="row text-secondary bread-crumb my-4">
                <div class="col-12">
                <a href="<?php echo site_url();?>" style="text-decoration:none;color: #6c757d !important;">Home</a> / <a href="<?php echo site_url();?>category" style="text-decoration:none;color: #6c757d !important;">Sản Phẩm</a> / <?php echo $product->category_name;?> /
                    <b class="text-active"><?php echo $product->name;?> </b>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 product-overview-images">
                    <div class="row">
                        <div class="col-3">
                            <div class="swiper-container gallery-thumbs">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <img class="img-fluid" src="assets/public/<?php echo $product->img1;?>" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="img-fluid" src="assets/public/<?php echo $product->img2;?>" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="img-fluid" src="assets/public/<?php echo $product->img3;?>" alt="">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="swiper-container gallery-top">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <img class="img-fluid" src="assets/public/<?php echo $product->img1;?>" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="img-fluid" src="assets/public/<?php echo $product->img2;?>" alt="">
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="img-fluid" src="assets/public/<?php echo $product->img3;?>" alt="">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 product-overview-info mt-5">
                    <div class="product-overview-info-wrap text-center">
                        <a href="" class="product-brand mb-2">Hãng Sản Xuất</a>
                        <h1 class="product-name my-2"><?php echo $product->name;?></h1>
                        <p class="text-active m-1">
                            <b>Thông tin chung:</b>
                        </p>
                        <p class="product-overview-info-desc mt-1" id="perfect-scrollbar-product-info"><?php echo $product->short_des;?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="product-details mb-3">
        <div class="container">
            <ul class="nav nav-tabs text-center justify-content-center" id="myTab" role="tablist">
                <li class="nav-item mx-3">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#product-details-desc" role="tab" aria-controls="product-details-desc"
                        aria-selected="true">Mô Tả Chi Tiết</a>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#product-details-brand-info" role="tab" aria-controls="product-details-brand-info"
                        aria-selected="false">Thông Tin Hãng Sản Xuất</a>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#product-details-question" role="tab" aria-controls="product-details-question"
                        aria-selected="false">Hỏi Đáp</a>
                </li>
            </ul>
            <div class="tab-content text-center py-5" id="myTabContent">
                <div class="tab-pane fade show active" id="product-details-desc" role="tabpanel" aria-labelledby="product-details-desc-tab">
                <?php echo $product->detail_des;?>
                </div>

                <div class="tab-pane fade" id="product-details-brand-info" role="tabpanel" aria-labelledby="product-details-brand-info-tab">
                    Chưa có thông tin
                </div>

                <div class="tab-pane fade" id="product-details-question" role="tabpanel" aria-labelledby="product-details-question-tab">
                    <div class="row text-left">
                        <div class="col-md-6">
                            <h3 class="product-details-question-title">
                                Chưa có câu hỏi cho <?php echo $product->name;?>
                            </h3>

                            <!-- <div class="product-details-question-item">
                                <div class="product-details-customer">
                                    <div class="user-profile-picture-wrap">
                                        <img src="img/user.jpg" alt="" class="product-details-customer-profile-picture">
                                    </div>
                                    <div class="product-details-customer-question">
                                        <p class="product-details-customer-name">
                                            <strong>Khách hàng 1</strong> -
                                            <span class="question-date">21 tháng 3, 2018</span>
                                        </p>
                                        <p class="product-details-customer-question-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae iure magni alias consectetur
                                            earum sequi distinctio eius maiores minus cumque aperiam modi tempora explicabo
                                            voluptatum, non ea? Iusto, accusantium provident.</p>
                                    </div>
                                </div>
                                <div class="product-details-admin">
                                    <div class="user-profile-picture-wrap">
                                        <img src="img/admin.jpg" alt="" class="product-details-admin-profile-picture">
                                    </div>
                                    <div class="product-details-admin-answer">
                                        <p class="product-details-admin-name">
                                            <strong>Admin</strong> -
                                            <span class="answer-date">22 tháng 3, 2018</span>
                                        </p>
                                        <p class="product-details-admin-answer-text">Lorem ipsum dolor sit amet consectetur elit. Adipisci laborum temporibus maiores
                                            quis accusantium aliquam cum corrupti, iste omnis, tenetur minima natus corporis
                                            sunt earum reiciendis sapiente velit vitae quia.</p>
                                    </div>
                                </div>
                                <hr>
                            </div> -->
                        
                        </div>
                        <div class="col-md-6 product-details-question-input">
                            <h3 class="product-details-question-title text-left">
                                Thắc mắc về sản phẩm ? Hỏi ngay!
                                ( tính năng chưa sử dụng được, chúng tôi xin lỗi vì sự bất tiện này)
                            </h3>

                            <p class="required-field-desc">Email của bạn sẽ không được công khai. Những trường bắt buộc được đánh dấu *.</p>
                            <form action="">
                                <label for="question">Câu hỏi của bạn *</label>
                                <textarea name="question" id="question" cols="30" rows="5" class="form-control" required disabled></textarea>
                                <div class="row mt-2">
                                    <div class="col-6">
                                        <label for="customer-name">Tên *</label>
                                        <input type="text" name=customer-name id=customer-name class="form-control" required disabled>
                                    </div>
                                    <div class="col-6">
                                        <label for="customer-email">Email *</label>
                                        <input type="text" name=customer-email id=customer-email class="form-control" required disabled>
                                    </div>
                                </div>
                                <button type=submit class="btn question-submit-btn" disabled>Gửi Câu Hỏi</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Footer -->
    <footer class="footer-distributed">
        <div class="container">


            <div class="footer-left">

                <h3>OtoNamSaiGonCo's
                    <span>logo</span>
                </h3>

                <p class="footer-links">
                    <a href="#">Home</a>
                    ·
                    <a href="#">Products</a>
                    ·
                    <a href="#">About Us</a>
                    ·
                    <a href="#">Đối Tác</a>

                </p>

                <p class="footer-company-name">OtoNamSaiGonCo &copy; 2018</p>
            </div>



            <div class="footer-center">
                <!-- Button trigger modal -->
                <ul>
                    <li>
                        <a href="">Đối tác miền Bắc</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Trung</a>
                    </li>
                    <li>
                        <a href="">Đối tác miền Nam</a>
                    </li>
                </ul>
                <button type="button" class="btn modal-btn" data-toggle="modal" data-target="#exampleModal">
                Liên hệ chúng tôi
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Liên hệ chúng tôi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="#" method="post">
                                    <input type="text" name="representator" class='partner-representator' required placeholder="Họ Tên" />
                                    <!-- <input type="text" name="company" class='partner-company' required placeholder="Tên Công Ty*" /> -->
                                    <input type="email" name="email" class='partner-email' required placeholder="Email*" />
                                    <input type="phone" name="phone" class='partner-phone' required placeholder="Số ĐT*" />
                                    <hr>
                                    <button type="button" class="btn" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn partner-registry-btn">Gửi</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-right">
                <div>
                    <i class="fa fa-map-marker"></i>
                    <p>
                        <span>Quận Thủ Đức</span> TP HCM, Việt Nam</p>
                </div>
                <div>
                    <i class="fa fa-phone"></i>
                    <p>+84 907 82 5486</p>
                </div>
                <div>
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <p>
                        <a href="mailto:support@company.com">Địa chỉ Facebook</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>


    </footer>
    <div class="side-btn">
        <a href="#head">
            <img src="statics/default/img/facebook_icon.png" id="fixedbutton">
        </a>
        <a href="" class="modal-btn" data-toggle="modal" data-target="#exampleModal">
            <div class="partner-register">
                <div class="partner-register-circle text-center">
                    <i class="fas fa-handshake"></i>
                    </i>
                </div>
                <div class="partner-register-desc">Đăng ký đối tác</div>
            </div>
        </a>
    </div>
    <button class="back-to-top-btn text-center" id="back-to-top-btn">
        <i class="fas fa-arrow-up"></i>
    </button>
    <!-- Footer -->
    <script src="statics/default/jquery/dist/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script src="statics/default/owl/dist/owl.carousel.min.js"></script>
    <script src="statics/default/animejs/anime.min.js"></script>
    <script src="statics/default/swiper/dist/js/swiper.min.js"></script>
    <script src="statics/default/perfect-scrollbar/dist/perfect-scrollbar.js"></script>
    <script src="statics/default/js/style.js"></script>

</body>

</html>