<div class="container">
        <div class="row section-title-wrap mb-4">
            <div class="section-title-line"></div>
            <h2 class="text-center section-title m-auto px-3">Đối tác</h2>
        </div>
    </div>
    <div class="container">
        <div class="owl-carousel featured-products-slide">
            <?php if ($partner) : ?>
                <?php foreach ($partner as $item) : ?>
                    <div class="justify-content-center">
                        <!-- <figure class="figure featured-products-item-figure"> -->
                                <div class="featured-products-item-wrap">
                                    <img class="" src="assets/partner/<?php echo $item->image ?>" alt="">
                                </div>
                        <!-- </figure> -->
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>