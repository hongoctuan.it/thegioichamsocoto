<style>
.parallax {
  /* The image used */
  background-image: url("assets/public/full.jpg");

  /* Set a specific height */
  min-height: 400px; 

  /* Create the parallax scrolling effect */
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
</style>


<div class="parallax">
  <div>&nbsp;<div>
  <div class="index-introduce">
    <div class="index-introduce-title"><?php echo $introduce_info->title;?></div>
    <div class="index-introduce-slogan"><?php echo $introduce_info->slogan;?></div>
    <div class="index-introduce-short_des"><?php echo $introduce_info->short_des;?></div>
  </div>
</div>

