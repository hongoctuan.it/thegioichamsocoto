<div class="container">
        <div class="row section-title-wrap mb-4" style="margin:0px !important">
            <div class="section-title-line"></div>
            <h2 class="text-center section-title m-auto px-3">Tin tức</h2>
        </div>
    </div>
    <div class="container">
        <div class="owl-carousel featured-products-slide">
            <?php if ($partner) : ?>
                <?php foreach ($news as $item) : ?>
                    <div class="justify-content-center">
                        <figure class="figure featured-products-item-figure">
                                <div class="featured-products-item-wrap">
                                    <img class="img-fluid" src="assets/news/<?php echo $item->img ?>" alt="">
                                    <?php echo $item->name ?>
                                    <?php 
                                        $str = strip_tags($item->description); //Lược bỏ các tags HTML
                                        if(strlen($str)>100) { //Đếm kí tự chuỗi $str, 100 ở đây là chiều dài bạn cần quy định
                                            $strCut = substr($str, 0, 90); //Cắt 100 kí tự đầu
                                            $str = substr($strCut, 0, strrpos($strCut, ' ')).'... <a href="'.site_url('news/').$item->slug.'" style="color:#da202c">Đọc thêm</a>'; //Tránh trường hợp cắt dang dở như "nội d... Read More"
                                        }
                                        echo $str;
                                    ?>
                                </div>
                        </figure>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
