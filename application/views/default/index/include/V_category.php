<div class="sub-home-category-wrap">
    <?php if(!empty($cats)):?>        
        <?php $dem = 0;?> 
        <?php foreach ($cats as $item) : ?>
            <? if(($dem%4)==0):?>
            <div class="sub-category-item">
                <div class="figure category-figure-item">
            <? endif;?>
                    <a href="<?php echo site_url('category/'.$item->slug) ?>">
                    <div class="category-item-title">
                        <div class="sub-category-service"><img src="<?php echo site_url('assets/category/'.$item->img) ?>" /></div>
                        <span style="line-height:40px;"><?php echo $item ? $item->name : ""; ?></span>
                    </div>
                    </a>
            <? if(($dem%4)==3):?>
                </div>
            </div>
            <?php endif?>
            <?php $dem++;?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>