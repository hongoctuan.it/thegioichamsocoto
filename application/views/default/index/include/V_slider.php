<div style="margin:0px;">
        <div class="row">
            <div class="owl-carousel home-banner-slide-carousel">
                <?php if ($banners) : ?>
                    <?php foreach ($banners as $banner) : ?>
                        <div class="row justify-content-center banner-wrap">
                            <a href='<?php echo $banner->link ? $banner->link : '#'; ?>'>
                                <img class="banner-image img-fluid" src="<?php echo base_url(); ?>assets/public/<?php echo $banner ? $banner->img : ''; ?>" alt="" style="margin:0px; height:500px;object-fit: cover;">
                            </a>
                            <div class="filter"></div>

                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- <div class="container">
        <div class="row">
            <div class="owl-carousel home-banner-slide-carousel" style="margin:0px; height:700px">
                <?php if ($banners) : ?>
                    <?php foreach ($banners as $banner) : ?>
                        <div class="row justify-content-center banner-wrap">
                            <a href='<?php echo $banner->link ? $banner->link : '#'; ?>'>
                                <img class="banner-image img-fluid" src="<?php echo base_url(); ?>assets/public/<?php echo $banner ? $banner->img : ''; ?>" alt="" style="margin:0px; height:500px">
                            </a>
                            <div class="filter"></div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div> -->
