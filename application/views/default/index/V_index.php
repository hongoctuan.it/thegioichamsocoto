<section class="home-banner-slide mb-3 pb-3">
    <?php include 'include/V_slider.php' ?>
</section>

<section class="home-category">
    <?php include 'include/V_category.php' ?>
</section>

<section class="home-feature">
    <div class="container">
        <div class="row section-title-wrap">
            <div class="section-title-line"></div>
            <h2 class="text-center section-title m-auto home-feature-title px-3">Vì sao chọn chúng tôi</h2>
        </div>
    </div>

    <p class="text-center section-description"><?php $hotproducts->descriptionfull; ?></p>
    <div class="container">
        <div class="row home-feature-wrap">
        <div class="col-lg-4 feature-item-wrap text-left">
                    <div class="mt-0"><?php echo $hotproducts->descriptionfull01; ?></div>
                    <div class="mt-0"><?php echo $hotproducts->descriptionfull02; ?></div>
            </div>
            <div class="col-lg-4 hover15 column align-items-center text-center m-0">
                <figure>
                    <iframe width="380px" height="380" src="https://www.youtube.com/embed/<?php echo $hotproducts->img ?>"frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                    <!-- <img src="<?php echo site_url("assets/home/" . $hotproducts->img) ?>" alt="feature" width=80% /> -->
                </figure>
            </div>
            <div class="col-lg-4 feature-item-wrap text-right">
                <div class="mt-0"><?php echo $hotproducts->descriptionfull03; ?></div>
                <div class="mt-0"><?php echo $hotproducts->descriptionfull04; ?></div>
            </div>
        </div>
    </div>
</section>

<section class="home-featured-products pt-4 mt-4">
    <?php include 'include/V_partner.php' ?>
</section>

<section class="home-featured-products">
    <?php include 'include/V_introduce.php' ?>
</section>
<section class="home-featured-products">
    <?php include 'include/V_news.php' ?>
</section>

<section class="home-featured-products">
    <?php include 'include/V_baogia.php' ?>
</section>