<section class="product-overview py-4">
    <div class="container">
        <div class="row text-secondary bread-crumb my-4">
            <div class="col-12">
                <a href="<?php echo site_url(); ?>" style="text-decoration:none;color: #6c757d !important;">Home</a> / <a href="<?php echo site_url(); ?>category" style="text-decoration:none;color: #6c757d !important;">Sản Phẩm</a> / <a href="<?php echo site_url("category/" . $product->category_slug); ?>" style="text-decoration:none;color: #6c757d !important;"><?php echo $product->category_name; ?></a> /
                <b class="text-active"><?php echo $product->name; ?> </b>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-12 product-overview-images">
                <div class="swiper-container gallery-top">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo 'assets/avatar/' . $product->img1; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo 'assets/avatar/' . $product->img2; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo 'assets/avatar/' . $product->img3; ?>')">
                        </div>
                    </div>
                    <div class="swiper-button-next swiper-button-black"></div>
                    <div class="swiper-button-prev swiper-button-black"></div>
                </div>
                <div class="swiper-container gallery-thumbs">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo 'assets/avatar/' . $product->img1; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo 'assets/avatar/' . $product->img2; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo 'assets/avatar/' . $product->img3; ?>')">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 product-overview-info mt-5">
                <div class=" text-center" style="padding:0px">
                    <h1 class="product-name my-2"><?php echo $product->name; ?></h1>
                    <p class='' style="text-align:left"><?php echo $product->short_des?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="product-details mb-1">
    <div class="container">
        <ul class="nav nav-tabs text-center justify-content-center" id="myTab" role="tablist">
            <li class="nav-item mx-3">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#product-details-desc" role="tab" aria-controls="product-details-desc" aria-selected="true">Mô Tả Chi Tiết</a>
            </li>

            <li class="nav-item mx-3">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#product-details-question" role="tab" aria-controls="product-details-question" aria-selected="false">Hỏi Đáp</a>
            </li>

        </ul>
        <div class="tab-content text-center py-2" id="myTabContent">
            <div class="tab-pane fade show active" id="product-details-desc" role="tabpanel" aria-labelledby="product-details-desc-tab">
                <?php echo $product->detail_des; ?>
            </div>



            <div class="tab-pane fade" id="product-details-question" role="tabpanel" aria-labelledby="product-details-question-tab">
                <div class="row text-left">
                    <div class="col-md-12 product-details-question-input">
                        <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-numposts="20"></div>

                    </div>
                </div>
            </div>
  
        </div>
    </div>
</section>
<section class="home-featured-products">
    <div class="container">
        <div class="row section-title-wrap mb-4">
            <div class="section-title-line"></div>
            <h2 class="text-center section-title m-auto px-3">Sản phẩm liên quan</h2>
        </div>
    </div>
    <div class="container p-5">
        <div class="owl-carousel featured-products-slide">
            <?php if ($relate_products) : ?>
            <?php foreach ($relate_products as $relate_product) : ?>
            <?php if ($relate_product->id !=$product->id) : ?>
            <div class="justify-content-center">
                <figure class="figure featured-products-item-figure">
                    <a href="<?php echo site_url('product/' . $relate_product->slug); ?>" class="featured-products-link">
                        <div class="featured-products-item-wrap">
                            <img class="featured-products-img img-fluid" src="<?php echo base_url();?>assets/avatar/<?php echo $relate_product->img1 ?>" alt="">
                            <figcaption class="figure-caption m-auto">
                                <h3 class="featured-products-title text-center text-dark mt-2">
                                    <?php echo $relate_product->name ?></h3>
                            </figcaption>
                        </div>
                    </a>
                </figure>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
