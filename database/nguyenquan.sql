-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2019 at 09:09 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nguyenquan`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `slogan` text COLLATE utf8_unicode_ci NOT NULL,
  `short_des` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_des` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `slogan`, `short_des`, `detail_des`, `slug`, `img`) VALUES
(1, '1', '2', '323', 'gioi_thieu', '1557255425_123.png');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `img` text COLLATE utf8_unicode_ci,
  `parent` int(11) NOT NULL DEFAULT '0',
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `img_large` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `description`, `img`, `parent`, `slug`, `active`, `deleted`, `level`, `img_large`) VALUES
(1, 'Hoá Phẩm', '', NULL, 0, 'hoa-pham', 1, 0, 0, '1558095411_img_content_header_home_use.jpg'),
(2, 'Mỹ Phẩm', NULL, NULL, 1, '', 1, 0, 1, ''),
(3, 'Gia Dụng', 'gia dung', NULL, 2, 'gia_dung', 1, 0, 2, ''),
(4, 'Thực Phẩm', NULL, NULL, 1, '', 1, 0, 1, ''),
(5, '1', '1', '1556798279_screen_shot_2019-03-24_at_23.01', 0, '', 1, 1, 0, ''),
(6, '2', '2', '1556798299_screen_shot_2019-04-02_at_21.32', 0, '', 1, 1, 0, ''),
(7, '3', '3', '1556798325_screen_shot_2019-03-24_at_23.01', 0, '', 0, 1, 0, ''),
(8, '4', '4', '1556800030_screen_shot_2019-03-24_at_19.56', 0, '', 1, 1, 0, ''),
(9, 'Ability', '6', '1556801555_screen_shot_2019-04-02_at_21.29', 0, '', 1, 1, 0, ''),
(10, '3', '2', '1556801574_screen_shot_2019-03-24_at_19.56', 0, '', 1, 1, 0, ''),
(11, '4', '6', '1556803262_screen_shot_2019-04-02_at_21.29', 3, 'so_4', 0, 0, 0, ''),
(12, '4', '5', '1556803092_screen_shot_2019-03-24_at_19.56', 3, '', 0, 1, 0, ''),
(13, 'Danh Mục 5', '<p>abc</p>\r\n', '1556999241_screen_shot_2019-03-24_at_19.56', 0, 'danh-muc-5', 1, 0, 0, '1558095359_1556022426_screenshot_181.png'),
(14, 'c', 'c', '1556999426_screen_shot_2019-03-24_at_19.56', 13, '', 1, 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` int(11) NOT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_updated` datetime NOT NULL,
  `previous_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `info`, `value`, `last_updated`, `previous_value`) VALUES
(1, 'address', 'Quận Thủ Đức TP HCM, Việt Nam', '2019-05-18 14:15:08', NULL),
(2, 'phone', '123456789', '2019-05-18 14:15:08', NULL),
(3, 'facebook', '#', '2019-05-18 14:15:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detaildistribution`
--

CREATE TABLE `detaildistribution` (
  `id` int(11) NOT NULL,
  `distributionId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `paymentId` int(11) NOT NULL,
  `paymentProduct` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distribution`
--

CREATE TABLE `distribution` (
  `id` int(11) NOT NULL,
  `distributorId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `totalPayment` int(11) NOT NULL DEFAULT '0',
  `userId` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distributor`
--

CREATE TABLE `distributor` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `name`, `description`, `img`, `active`, `slug`, `deleted`) VALUES
(1, 'Album 1', 'Đây là album 1', '1557089611_screen_shot_2019-03-24_at_23.01', 1, 'thu_vien', 0),
(2, 'Album 2', 'Đây là album 2', '1557089705_screen_shot_2019-03-24_at_23.01', 0, '', 1),
(3, 'Album 3', 'Đây là album 3', '1557090736_screen_shot_2019-03-24_at_23.01', 1, '', 0),
(4, 'Album 4', 'Đây là album 4', '1557090803_screen_shot_2019-03-24_at_23.01', 0, '', 1),
(5, 'Album 5', 'Đây là album 5', '1557090825_screen_shot_2019-04-02_at_21.29', 0, '', 1),
(6, 'Album 6', 'Đây là album 6', '1557090855_screen_shot_2019-04-02_at_21.32', 0, '', 1),
(7, 'Album 7', 'Đây là album 7', '1557090871_screen_shot_2019-03-24_at_23.01', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_detail`
--

CREATE TABLE `gallery_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gallery_detail`
--

INSERT INTO `gallery_detail` (`id`, `name`, `description`, `img`, `gallery_id`, `active`, `slug`, `deleted`) VALUES
(3, 'hình ba', 'Đây là hình ba', '1546939759_hydrangeas.jpg', 1, 1, 'hinh_anh', 0),
(7, 'hình 7', 'Đây là hình 7', '1556021578_1531384150512-_mg_4418.jpg', 3, 1, '', 0),
(8, 'hình 8', 'Đây là hình 8', '1547413320_jellyfish.jpg', 1, 1, 'hinh_anh', 0),
(9, 'hình 9', 'Đây là hình 9', '1547413320_desert.jpg', 1, 1, 'hinh_anh', 0),
(10, 'hình 10', 'Đây là hình 10', '1547420364_desert11.jpg', 1, 1, 'hinh_anh', 0),
(11, 'hình 11', 'Đây là hình 11', '1556022835_17_-_vien_lam_sach_long_may_giat_2.jpg', 3, 1, '', 0),
(12, 'hình 12', 'Đây là hình 12', '1556023560_12_giay_uot_lau_mat_11.jpg', 3, 1, '', 0),
(13, 'hình 13', 'Đây là hình 13', '1556565686_1556022984_screenshot_15_7.png', 3, 1, '', 0),
(14, 'hình 14', 'Đây là hình 14', '1556565617_1556022579_5_tay_dang_kem_2.JPG', 3, 1, '', 0),
(15, 'hình 7', 'Đây là hình 7', '1556023533_1531383476327-12_2.png', 3, 1, '', 0),
(16, 'hình 7', 'Đây là hình 7', '1556021578_1531384150512-_mg_4418.jpg', 3, 1, '', 0),
(17, 'hình 8', 'Đây là hình 8', '1556022696_1531384656467-_mg_4368.jpg', 1, 1, 'hinh_anh', 0),
(18, 'hình 9', 'Đây là hình 9', '1556021072_1_2_3_4_dd_tay_da_nang_huong_trai_cay_1.JPG', 1, 1, 'hinh_anh', 0),
(19, 'hình 10', 'Đây là hình 10', '1556023560_12_giay_uot_lau_mat_11.jpg', 1, 1, 'hinh_anh', 0),
(20, 'hình 11', 'Đây là hình 11', '1556022835_17_-_vien_lam_sach_long_may_giat_2.jpg', 3, 1, '', 0),
(21, 'hình 3', 'Đây là hình 3', '1556022834_17_-_vien_lam_sach_long_may_giat_1.jpg', 1, 1, 'hinh_anh', 0),
(22, 'hình 7', 'Đây là hình 7', '1556565656_1556022426_screenshot_15_6.png', 3, 1, '', 0),
(23, 'hình 8', 'Đây là hình 8', '1557228468_slide11.jpg', 1, 1, 'hinh_anh', 0),
(24, 'hình 9', 'Đây là hình 9', '1556021578_1531384140497-_mg_4379.jpg', 1, 1, 'hinh_anh', 0),
(25, 'hình 11', 'Đây là hình 11', '1556022426_screenshot_181.png', 3, 1, '', 0),
(26, 'hình 12', 'Đây là hình 12', '1556023533_1531383447888-_mg_44961.jpg', 3, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `time` text COLLATE utf8_unicode_ci NOT NULL,
  `des` text COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `title`, `time`, `des`, `img`, `deleted`) VALUES
(1, '15', '23', '34', '1557184038_123.png', 0),
(2, '1', '2', '2', '1557184176_screen_shot_2019-03-24_at_23.01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_banner`
--

CREATE TABLE `home_banner` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promotion_id` int(11) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) DEFAULT '1',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_banner`
--

INSERT INTO `home_banner` (`id`, `name`, `promotion_id`, `img`, `active`, `deleted`, `link`, `description`) VALUES
(1, 'Banner 1', 1, '1556574471_home-banner-slide.jpg', 1, 0, NULL, NULL),
(2, 'Banner 2', 1, '1556574727_home-banner-slide.jpg', 0, 1, NULL, NULL),
(3, 'Banner 2', 1, '1556575087_home-banner-slide2.jpg', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_hot_product`
--

CREATE TABLE `home_hot_product` (
  `id` int(11) NOT NULL,
  `img` int(11) NOT NULL,
  `des_full` text COLLATE utf8_unicode_ci NOT NULL,
  `des_01` text COLLATE utf8_unicode_ci NOT NULL,
  `des_02` text COLLATE utf8_unicode_ci NOT NULL,
  `des_03` text COLLATE utf8_unicode_ci NOT NULL,
  `des_04` text COLLATE utf8_unicode_ci NOT NULL,
  `des_05` text COLLATE utf8_unicode_ci NOT NULL,
  `des_06` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_hot_product`
--

INSERT INTO `home_hot_product` (`id`, `img`, `des_full`, `des_01`, `des_02`, `des_03`, `des_04`, `des_05`, `des_06`) VALUES
(1, 1558446179, '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `home_info_compa`
--

CREATE TABLE `home_info_compa` (
  `id` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `district` int(11) NOT NULL,
  `agency` int(11) NOT NULL,
  `product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_info_compa`
--

INSERT INTO `home_info_compa` (`id`, `area`, `district`, `agency`, `product`) VALUES
(1, 2, 1, 23, 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `parent` int(11) NOT NULL DEFAULT '0',
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `description`, `img`, `active`, `parent`, `slug`, `deleted`, `created_at`, `created_by`) VALUES
(1, 'Tin Tức Chung', 'bn', '1556565617_1556022579_5_tay_dang_kem_2.JPG', 1, 0, 'tin-tuc-chung', 0, '2019-05-22 07:07:06', NULL),
(2, 'Tin Cty', 'a', '1556565782_1556023560_12_giay_uot_lau_mat_1.jpg', 1, 0, 'tin-cty', 0, '2019-05-22 07:07:06', NULL),
(3, 'Tin Khuyến Mãi', 'c', '1556021072_1_2_3_4_dd_tay_da_nang_huong_trai_cay_1.JPG', 1, 0, 'tin-khuyen-mai', 0, '2019-05-22 07:07:06', NULL),
(4, 'Tin Tức 1', 'Đây là tin tức 1', '1556021072_1_2_3_4_dd_tay_da_nang_huong_trai_cay_1.JPG', 1, 3, 'tin-tuc-1', 0, '2019-05-22 07:07:06', NULL),
(5, 'Tin Tức 2', 'Đây là tin tức 2', '1556565782_1556023560_12_giay_uot_lau_mat_1.jpg', 1, 1, 'tin_tuc_2', 0, '2019-05-22 07:07:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `area` int(50) NOT NULL,
  `latitude` int(11) NOT NULL DEFAULT '0',
  `longitude` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`id`, `name`, `address`, `phone`, `area`, `latitude`, `longitude`, `active`, `slug`, `deleted`) VALUES
(1, 'cua hang 1', 'dong nai', '1234', 1, 1, 2, 0, '', 0),
(2, '1', '1', '1', 0, 1, 1, 1, 'cua_hang', 0);

-- --------------------------------------------------------

--
-- Table structure for table `partner_registry`
--

CREATE TABLE `partner_registry` (
  `id` int(11) NOT NULL,
  `representator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `register_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `producer`
--

CREATE TABLE `producer` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `short_des` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_des` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `producerId` int(11) DEFAULT NULL,
  `img3` text COLLATE utf8_unicode_ci,
  `img2` text COLLATE utf8_unicode_ci,
  `img1` text COLLATE utf8_unicode_ci,
  `event_id` int(11) DEFAULT NULL,
  `price` int(20) NOT NULL DEFAULT '0',
  `promotion_id` int(11) DEFAULT NULL,
  `inStock` int(11) NOT NULL DEFAULT '0',
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `hot` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `img4` text COLLATE utf8_unicode_ci,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `short_des`, `detail_des`, `category_id`, `producerId`, `img3`, `img2`, `img1`, `event_id`, `price`, `promotion_id`, `inStock`, `featured`, `hot`, `active`, `img4`, `slug`, `deleted`) VALUES
(5, 'sp1', 'short des sp1', 'detail des sp1', 1, NULL, '', '', '', NULL, 50000, NULL, 1, 0, 1, 1, NULL, '', 1),
(6, 'Ability clean hontai (Dung dịch tẩy đa năng hương trái cây ) chai 500ml', 'Tẩy được tất cả các vết bẩn trong ngôi nhà bạn.', 'Tẩy được tất cả các vết bẩn trong ngôi nhà bạn như : các vết dơ khó tẩy trên các thiết bị nhà bếp , các thiết bị vệ sinh , tẩy dầu mỡ , khói bám và các thiết bị nhà bạn hay công xưởng  (máy hút khói ,quạt hút máy móc …. ) tẩy màng cửa ,tẩy quần áo ... Chai 500ml, quy cách 24 chai / thùng', 1, NULL, '1556021072_1_2_3_4_dd_tay_da_nang_huong_trai_cay_1.JPG', '1556021071_1531370885006-_mg_4306.jpg', '1556021071_1531370878095-_mg_4305.jpg', NULL, 0, NULL, 0, 1, 0, 1, NULL, 'nuoc_rua_chen', 0),
(7, 'Ability clean hontai (Dung dịch tẩy đa năng hương trái cây ) gói 400ml', 'Tẩy được tất cả các vết bẩn trong ngôi nhà bạn', 'Tẩy được tất cả các vết bẩn trong ngôi nhà bạn như : các vết dơ khó tẩy trên các thiết bị nhà bếp , các thiết bị vệ sinh , tẩy dầu mỡ , khói bám và các thiết bị nhà bạn hay công xưởng  (máy hút khói ,quạt hút máy móc …. ) tẩy màng cửa ,tẩy quần áo ... Gói 400ml, quy cách 20 gói / 1 thùng', 1, NULL, '', '1556021173_1531455469953-_mg_4313.jpg', '1556021173_1531455461732-_mg_4308.jpg', NULL, 0, NULL, 0, 1, 0, 1, NULL, '', 0),
(8, 'Orange oil cleaner (dung dịch tẩy dầu mỡ hương cam ) chai 400ml', 'Đánh bay những vết dầu mỡ bám vào các thiết bị nhà bếp của bạn.', 'Đánh bay những vết dầu mỡ bám vào các thiết bị nhà bếp của bạn.\r\nTẩy những vết bẩn bám vào các thiết bị vệ sinh nhà bạn mà không hư men , không ăn mòn inox và đồng thời không làm hại da tay vì được chiết xuất từ quả cam. Chai 400ml, quy cách : 24 chai / thùng.', 1, NULL, '1556021642_1531384299492-_mg_4374.jpg', '1556021642_1531384292457-_mg_4377.jpg', '1556021642_1531384286030-_mg_4373.jpg', NULL, 0, NULL, 0, 1, 0, 1, NULL, '', 0),
(9, 'Orange oil cleaner (dung dịch tẩy dầu mỡ hương cam gói bổ sung)', 'Đánh bay những vết dầu mỡ bám vào các thiết bị nhà bếp của bạn.', 'Đánh bay những vết dầu mỡ bám vào các thiết bị nhà bếp của bạn.\r\nTẩy những vết bẩn bám vào các thiết bị vệ sinh nhà bạn mà không hư men , không ăn mòn inox và đồng thời không làm hại da tay vì được chiết xuất từ quả cam. Gói 350ml, quy cách : 20 gói / thùng.', 1, NULL, '1556021578_1531384150512-_mg_4418.jpg', '1556021578_1531384146285-_mg_4381.jpg', '1556021578_1531384140497-_mg_4379.jpg', NULL, 0, NULL, 0, 0, 0, 1, NULL, '', 0),
(10, 'Dung dịch vệ sinh và diệt khuẩn', 'Đây là loại phun vô trùng với nồng độ ethanol 58% an toàn cho sử dụng thực phẩm vì nó là phụ gia thực phẩm , và diệt khuẩn lên đến 99,99%.', 'Chai 420ml, quy cách : 24 chai / thùng.', 1, NULL, '', '', '1556021841_screenshot_15.png', NULL, 0, NULL, 0, 0, 0, 1, NULL, '', 0),
(11, 'Dung dịch lau chùi gỗ ', 'Với chất tẩy rửa trung trính ,đánh bay các vết bẩn mà không làm hư gỗ và đỗi màu gỗ.', 'Chai 400ml, quy cách 24 chai / thùng.', 1, NULL, '', '', '1556021957_screenshot_15_2.png', NULL, 0, NULL, 0, 0, 0, 1, NULL, '', 0),
(12, 'Dung dịch vệ sinh phòng tắm', 'Với chất tẩy rửa trung trính ,đánh bay các vết bẩn mà không làm hư và rỉ sét các thiết bị nhà bạn.', 'Chai 400ml, quy cách : 24 chai / thùng.', 1, NULL, '', '', '1556022071_screenshot_15_3.png', NULL, 0, NULL, 0, 1, 0, 1, NULL, '', 0),
(13, 'Bột vệ sinh và diệt khuẩn', 'Là loại baking soda nở trong nước và hòa tan tinh khiết trong nước .', 'Là loại baking soda nở trong nước và hòa tan tinh khiết trong nước .Có tính năng đánh bật các mãn bám ,bụi bẩn và khử mùi đường ống ,rác thô ,đánh bóng các mãn bám trên inox (ấm đung nước ,nồi v.v.). Gói 1kg, quy cách : 12 gói / thùng.', 1, NULL, '', '', '1556022182_screenshot_15_4.png', NULL, 0, NULL, 0, 1, 0, 1, NULL, '', 0),
(14, 'Nước lau sàn nhật bản', 'Nước lau sàn thế hệ mới. không màu, không mùi .lau nhà nhanh hơn và sạch hơn.', 'Chai 1000ml, quy cách : 8 chai / thùng.', 1, NULL, '', '', '1556022259_screenshot_15_5.png', NULL, 0, NULL, 0, 1, 0, 1, NULL, '', 0),
(15, 'Mút cọ rửa đa năng ', 'KHÔNG CẦN DÙNG CHẤT TẨY RỬA, chỉ cần thấm nước vào miếng mouse và lau. Đặc biệt đánh bay các vết \r\nbẩn cứng đầu như các vết bợn trà, café bám lâu ngày trên ly tách.', ' *Lưu ý không dùng sản phẩm để chùi các bề mặt như thân xe, sơn mài, sản phẩm có phủ chất bóng coating vì có thể làm mất lớp bóng. Có thể cắt bằng kéo thông thường theo kích thước sử dụng. Quy cách : 30 miếng / thùng.', 3, NULL, '', '1556565656_1556022426_screenshot_18.png', '1556565656_1556022426_screenshot_15_6.png', NULL, 0, NULL, 0, 0, 0, 1, NULL, '', 0),
(16, 'Kem tẩy đa năng ', 'Kem tẩy đa năng với thành phần được chiết xuất thiên nhiên từ trái dừa, trái thơm, trái đu đủ. Đánh bay tất cả các vết bẩn, rỉ sét cứng đầu một cách dễ dàng và nhanh chóng.', 'Hũ 100gr, quy cách : 48 hũ / thùng.', 1, NULL, '1556565617_1556022579_5_tay_dang_kem_2.JPG', '1556565617_1556022579_1531390677194-_mg_4346.jpg', '1556565617_1556022579_1531390672030-_mg_4445.jpg', NULL, 0, NULL, 0, 0, 0, 1, NULL, '', 0),
(17, 'Dung dịch làm sạch và loại trừ ẩm mốc ', 'Với thành phần đặt biệt dung dịch làm sạch và loại được tất cả các vết ẩm mốc trong ngôi nhà bạn một cách dễ dàng và nhanh chóng.', 'Chai 180ml, quy cách : 30 chai / thùng.', 1, NULL, '1556022696_1531384662130-_mg_4370.jpg', '1556022696_1531384668869-_mg_4371.jpg', '1556022696_1531384656467-_mg_4368.jpg', NULL, 0, NULL, 0, 0, 0, 1, NULL, '', 0),
(18, 'Viên làm sạch lồng giặt Ag  (ion bạc)', '* Pha trộn ion bạc và sức mạnh diệt khuẩn cao !! Diệt khuẩn, làm sạch lồng giặt, khử mùi ẩm mốc.', '1 vỉ 8 viên, quy cách : 18 viên x 8 vỉ (144 viên).', 1, NULL, '1556022835_17_-_vien_lam_sach_long_may_giat_2.jpg', '1556022834_17_-_vien_lam_sach_long_may_giat_1.jpg', '1556022834_1531382002626-_mg_4524.jpg', NULL, 0, NULL, 0, 0, 0, 1, NULL, '', 0),
(19, 'Thảm phòng tránh rận', 'Được sản xuất từ vải không dệt và được tẩm tinh dầu copaiba không mùi giúp dễ chịu khi sử dụng ngoài ra tinh dầu copaiba có tính năng phòng tránh và xua đuổi được các loại rận, kiến, côn trùng tới gần bé yêu nhà bạn.', '3 tấm / hộp, quy cách : 48 hộp / thùng.', 3, NULL, '', '', '1556565686_1556022984_screenshot_15_7.png', NULL, 0, NULL, 0, 0, 0, 1, NULL, '', 0),
(20, 'Khăn Vải Không dệt làm sạch nhà bếp', 'Làm sạch và diệt khuẩn bếp không hại da tay người dùng. ', 'Làm sạch và diệt khuẩn bếp không hại da tay người dùng. Được tẩm tinh dầu cam sẽ làm tan chảy những chất như dầu mỡ, chất dơ.... Mgoài ra sự điện phân nước được sử dụng trong sản phẩm này được cấu thành từ sự phân giải điện dung dịch kali cacbonat ở mức phụ gia thực phẩm làm phân giải các chất dơ trên bề mặt đồ dùng. Sản phẩm này thân thiện với môi trường và da tay người dùng. Quy cách : 30 gói / thùng (1 gói = 22 miếng).', 3, NULL, '', '', '1556565710_1556023103_product5.png', NULL, 0, NULL, 0, 0, 0, 1, NULL, '', 0),
(21, 'Khăn Vải Không dệt làm sạch phòng ngủ', 'Làm sạch và diệt khuẩn phòng ngủ.', 'Làm sạch và diệt khuẩn phòng ngủ. Sự điện phân nước được sử dụng trong sản phẩm này được cấu thành từ sự phân giải điện dung dịch kali cacbonat ở mức phụ gia thực phẩm làm phân giải các chất dơ trên bề mặt đồ dùng. Ngoài ra chất benzalkonium chloride là chất có tính khử độc và diệt khuẩn được sử dụng làm sạch môi trường. Quy cách : 30 gói / thùng (1gói = 22 miếng).', 3, NULL, '', '', '1556565729_1556023169_product6.png', NULL, 0, NULL, 0, 1, 0, 1, NULL, '', 0),
(22, 'Khăn Vải Không dệt làm sạch nhà bếp (20 miếng)', 'Làm sạch và diệt khuẩn bếp không hại da tay người dùng.', 'Làm sạch và diệt khuẩn bếp không hại da tay người dùng. Được tẩm tinh dầu cam sẽ làm tan chảy nhửng chất như dầu,mỡ,chất dơ v.v.ngoài ra sự điện phân nước được sử dụng trong sản phẩm này được cấu thành từ sự phân giải điện dung dịch kali cacbonat ở mức phụ gia thực phẩm làm phân giải các chất dơ trên bề mặt đồ dùng. Sản phẩm này thân thiện với môi trường và da tay người dùng. Quy cách : 48 gói / 1 thùng (1gói = 20 miếng).', 3, NULL, '', '', '1556565753_1556023351_screenshot_15_8.png', NULL, 0, NULL, 0, 1, 0, 1, NULL, '', 0),
(23, 'Khăn Vải Không dệt làm sạch phòng ngủ (20 miếng)', 'Làm sạch và diệt khuẩn phòng ngủ ', 'Làm sạch và diệt khuẩn phòng ngủ. Sự điện phân nước được sử dụng trong sản phẩm này được cấu thành từ sự phân giải điện dung dịch kali cacbonat ở mức phụ gia thực phẩm làm phân giải các chất dơ trên bề mặt đồ dùng. Ngoài ra chất benzalkonium chloride là chất có tính khử độc và diệt khuẩn được sử dụng làm sạch môi trường. Quy cách :  48 gói / thùng (1gói = 20 miếng).', 3, NULL, '', '', '1556565765_1556023453_product4.jpg', NULL, 0, NULL, 0, 0, 1, 1, NULL, '', 0),
(24, 'Khăn Vải Không dệt làm tỉnh ngủ', 'Với tinh dầu khuynh diệp và bạc hà tạo cảm giác sản khoái tỉnh táo khi sử dụng.', 'Quy cách : 90 gói /  thùng.', 3, NULL, '1556565782_1556023560_12_giay_uot_lau_mat_1.jpg', '1556565782_1556023533_1531383447888-_mg_4496.jpg', '1556565782_1556023533_1531383438606-_mg_4493.jpg', NULL, 0, NULL, 0, 0, 1, 1, NULL, '', 0),
(25, '1', '1', '1', 1, NULL, '1556896479_screen_shot_2019-04-02_at_21.33', '1556896479_screen_shot_2019-04-02_at_21.29', '1556896479_screen_shot_2019-03-24_at_19.56', NULL, 0, NULL, 0, 0, 1, 1, '1556896575_screen_shot_2019-04-03_at_22.05', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `starDate` datetime NOT NULL,
  `endDate` datetime NOT NULL,
  `discount` float NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `gift_productId` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `name`, `description`, `starDate`, `endDate`, `discount`, `type`, `gift_productId`, `deleted`) VALUES
(1, 'asd', 'asdas', '2019-04-23 00:00:00', '2019-04-30 00:00:00', 1235, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `repcomment`
--

CREATE TABLE `repcomment` (
  `id` int(11) NOT NULL,
  `commentId` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `full_name`, `email`, `phone`, `role`, `deleted`) VALUES
(2, 'root', '984c0b92ff9d5232d23345621d90b8d882764896', 'qưe', 'hongoctuan.it@gmail.com', '090909', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `category` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `category` ADD FULLTEXT KEY `name_2` (`name`,`description`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commentProduct` (`productId`);

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detaildistribution`
--
ALTER TABLE `detaildistribution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detaildistributionDitributon` (`distributionId`),
  ADD KEY `fk_detaildistributionProduct` (`productId`);

--
-- Indexes for table `distribution`
--
ALTER TABLE `distribution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_distributionDistributor` (`distributorId`),
  ADD KEY `fk_distributionUser` (`userId`);

--
-- Indexes for table `distributor`
--
ALTER TABLE `distributor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallery_id` (`gallery_id`);
ALTER TABLE `gallery_detail` ADD FULLTEXT KEY `name` (`name`,`description`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banner`
--
ALTER TABLE `home_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `news` ADD FULLTEXT KEY `name` (`name`,`description`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `partner` ADD FULLTEXT KEY `name` (`name`,`address`);
ALTER TABLE `partner` ADD FULLTEXT KEY `name_2` (`name`,`address`);
ALTER TABLE `partner` ADD FULLTEXT KEY `name_3` (`name`,`address`);

--
-- Indexes for table `partner_registry`
--
ALTER TABLE `partner_registry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `producer`
--
ALTER TABLE `producer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producer` (`producerId`),
  ADD KEY `fk_category` (`category_id`),
  ADD KEY `fk_event` (`event_id`),
  ADD KEY `fk_promotion` (`promotion_id`);
ALTER TABLE `product` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `product` ADD FULLTEXT KEY `name_2` (`name`,`detail_des`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repcomment`
--
ALTER TABLE `repcomment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_repcommentComment` (`commentId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `detaildistribution`
--
ALTER TABLE `detaildistribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `distribution`
--
ALTER TABLE `distribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `distributor`
--
ALTER TABLE `distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `home_banner`
--
ALTER TABLE `home_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `partner_registry`
--
ALTER TABLE `partner_registry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `producer`
--
ALTER TABLE `producer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `repcomment`
--
ALTER TABLE `repcomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_commentProduct` FOREIGN KEY (`productId`) REFERENCES `product` (`id`);

--
-- Constraints for table `detaildistribution`
--
ALTER TABLE `detaildistribution`
  ADD CONSTRAINT `fk_detaildistributionDitributon` FOREIGN KEY (`distributionId`) REFERENCES `distribution` (`id`),
  ADD CONSTRAINT `fk_detaildistributionProduct` FOREIGN KEY (`productId`) REFERENCES `product` (`id`);

--
-- Constraints for table `distribution`
--
ALTER TABLE `distribution`
  ADD CONSTRAINT `fk_distributionDistributor` FOREIGN KEY (`distributorId`) REFERENCES `distributor` (`id`),
  ADD CONSTRAINT `fk_distributionUser` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Constraints for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  ADD CONSTRAINT `gallery_detail_ibfk_1` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `fk_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `fk_producer` FOREIGN KEY (`producerId`) REFERENCES `producer` (`id`),
  ADD CONSTRAINT `fk_promotion` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`id`);

--
-- Constraints for table `repcomment`
--
ALTER TABLE `repcomment`
  ADD CONSTRAINT `fk_repcommentComment` FOREIGN KEY (`commentId`) REFERENCES `comment` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
